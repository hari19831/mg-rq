'use strict';

//Ref: https://docs.microsoft.com/en-us/javascript/api/overview/azure/key-vault-index?view=azure-node-latest

//Imports

var keyClient = require('./vaultAuthentication').vaultClient_Key;

module.exports.keyClient = keyClient;