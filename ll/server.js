const { DefaultAzureCredential, ClientSecretCredential, ChainedTokenCredential } = require("@azure/identity");
const btoa = require('btoa');
const atob = require('atob');

/*
    inlpvault Service Principal Secret
    U4tj-X88Mdy___fK.SZ8-9i8vrK61zxC2v
    07658519-a7a6-4f58-854a-5941a96a6025

    ProdEnggVault
    OU_FI3Qirg_I~l64Tj896.V13Fziim6U~l
    49c8a8bc-fa84-4501-94bb-b1df841880b7

    inlp-LangauageDetection_Prinipal
    oZqRZ96nG~hKlpAdsA-k-2e4cD.zh.13ra
    ab088466-3f05-4fe8-9f2b-63a193b6d525
*/

// When an access token is requested, the chain will try each
// credential in order, stopping when one provides a token

//var tenantID = `5d9ff9cf-f69a-4381-a556-dccb0cb8da5f`;
//var clientID = `3887d3f6-4b5b-4d12-967e-991df55477f0`;
//var clientSecret = `OU_FI3Qirg_I~l64Tj896.V13Fziim6U~l`;
//var vaultURL = `https://prodenggvault.vault.azure.net/`;

var tenantID = `5d9ff9cf-f69a-4381-a556-dccb0cb8da5f`;
var clientID = `4fcce3fe-bac6-4939-bbf7-a7462d7730de`;
var clientSecret = `U4tj-X88Mdy___fK.SZ8-9i8vrK61zxC2v`;
var vaultURL = `https://inlpvault.vault.azure.net/`;

const firstCredential = new ClientSecretCredential(tenantID, clientID, clientSecret);
//const secondCredential = new ClientSecretCredential(tenantId, anotherClientId, anotherSecret);
//const credentialChain = new ChainedTokenCredential(firstCredential, secondCredential);
console.log(firstCredential);
// The chain can be used anywhere a credential is required
const { KeyClient } = require("@azure/keyvault-keys");
const { SecretClient } = require("@azure/keyvault-secrets");
const client = new SecretClient(vaultURL, firstCredential);

console.log(client);

const secretName = "MySecretName";

//async function main() {
//  const result = await client.setSecret(secretName, "MySecretValue");
//  console.log("result: ", result);
//}
//
//main();

//const client = new KeyClient(url, credential);
//
//const keyName = "MyKeyName";
//
//async function main() {
//  const result = await client.createKey(keyName, "RSA");
//  console.log("result: ", result);
//}
//
//main();

console.log(btoa(`5d9ff9cf-f69a-4381-a556-dccb0cb8da5f`));
console.log(btoa(`4fcce3fe-bac6-4939-bbf7-a7462d7730de`));
console.log(btoa(`U4tj-X88Mdy___fK.SZ8-9i8vrK61zxC2v`));
console.log(btoa(`prodenggvault`));
//console.log(atob('c2RkYQ=='));