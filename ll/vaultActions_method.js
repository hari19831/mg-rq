const { secretClient } = require('./vaultActions_Secret');

var setSecret = (secretName, secretValue) => {
    return new Promise(async (resolve, reject) => {
        try {
            const result = await secretClient.client.setSecret(secretClient.vaultUrl,secretName, secretValue);
            resolve(result);
        } catch (error) {
            reject(error);
        }
    });
}

var getSecret = (secretName) => {
    return new Promise(async (resolve, reject) => {
        try {
            const result = await secretClient.client.getSecret(secretClient.vaultUrl,secretName,"");
            resolve(result.value);
        } catch (error) {
            reject(error);
        }
    });
}

var deleteSecret = (secretName) => {
    return new Promise(async (resolve, reject) => {
        try {
            await secretClient.client.beginDeleteSecret(secretName);
            resolve(true);
        } catch (error) {
            reject(error);
        }
    });
}

module.exports.setSecret = setSecret;
module.exports.getSecret = getSecret;
module.exports.deleteSecret = deleteSecret;