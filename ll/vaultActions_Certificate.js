'use strict';

//Ref: https://docs.microsoft.com/en-us/javascript/api/overview/azure/key-vault-index?view=azure-node-latest

//Imports

var certificateClient = require('./vaultAuthentication').vaultClient_Certificate;

module.exports.certificateClient = certificateClient;