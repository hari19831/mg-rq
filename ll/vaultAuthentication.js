'use strict';

var { ClientSecretCredential } = require("@azure/identity");

var { KeyClient } = require("@azure/keyvault-keys");
var { SecretClient } = require("@azure/keyvault-secrets");
var { CertificateClient } = require("@azure/keyvault-certificates");

//Imports
var vaultConfig = require('./vaultConfig.json');
//console.log(vaultConfig);
var authenticationMask = require('./authenticationMask.js');

var vaultClient_Key;
var vaultClient_Secret;
var vaultClient_Certificate;


var tenantID = authenticationMask.decodeToken(vaultConfig.tenantID);
var clientID = authenticationMask.decodeToken(vaultConfig.appClientID);
var clientSecret = authenticationMask.decodeToken(vaultConfig.clientSecret);
var vaultName = authenticationMask.decodeToken(vaultConfig.vaultName);

var vaultURL = `https://${vaultName}.vault.azure.net/`;
var vaultCredential = new ClientSecretCredential(tenantID, clientID, clientSecret);

vaultClient_Key = new KeyClient(vaultURL, vaultCredential);
vaultClient_Secret = new SecretClient(vaultURL, vaultCredential);
vaultClient_Certificate = new CertificateClient(vaultURL, vaultCredential);

//Exports
module.exports.vaultClient_Key = vaultClient_Key;
module.exports.vaultClient_Secret = vaultClient_Secret;
module.exports.vaultClient_Certificate = vaultClient_Certificate;
