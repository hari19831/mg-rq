'use strict';

const btoa = require('btoa');
const atob = require('atob');

function encodeToken(token){
    let tokenValue = token || ``;
    try{
        return  btoa(tokenValue);
    }catch(err){
        console.log(err);
        return ``;
    }
}

function decodeToken(token){
    let tokenValue = token || ``;
    try{
        return atob(tokenValue);
    }catch(err){
        console.log(err);
        return ``;
    }
}

module.exports.encodeToken = encodeToken;
module.exports.decodeToken = decodeToken;