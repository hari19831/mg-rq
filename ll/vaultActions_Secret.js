'use strict';

//Ref: https://docs.microsoft.com/en-us/javascript/api/overview/azure/key-vault-index?view=azure-node-latest

//Imports

var secretClient = require('./vaultAuthentication').vaultClient_Secret;

module.exports.secretClient = secretClient;