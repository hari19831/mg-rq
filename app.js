'use strict';
var express      = require('express');
var path         = require('path');
var cookieParser = require('cookie-parser');
var logger       = require('morgan');
var http         = require("http");
var helmet       = require("helmet") 
var socketio     = require("socket.io");

const { instrument } = require("@socket.io/admin-ui");

//---Imports---||
var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var brokerFeatureImport  = require('./routes/brokerFeatureRouter');
var wrapperFeatureImport = require('./routes/wrapperFeatureRouter');
var brokerValidationImport = require('./routes/brokerValidationRouter');

var app = express();

//--------------------------------Socket-Initialization-----------------------------------||

const server = require('http').createServer(app); 
const io = require("socket.io")(server,{ 
                                            cors:{ 
                                                    origin: "*", 
                                                    methods: ["GET", "POST"],
                                                    credentials: true 
                                                },
                                            pingTimeout: 50000,     //setTimeout limit for large payload
                                            maxHttpBufferSize: 5e6  //setmax byte size for payload
                                        });

instrument(io, {
    auth: {
        type: "basic",
        username: "admin",
        password: "$2b$10$heqvAkYMez.Va6Et2uXInOnkCT6/uQj1brkrbyG3LpopDklcq7ZOS" // "changeit" encrypted with bcrypt
    },
    });

//--------------------------------Socket-Initialization-----------------------------------||



app.use(logger('dev'));
app.use(helmet());
app.use(express.json({limit: '50mb'}));
app.use(express.urlencoded({ extended: false, limit: '100mb'}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/users', usersRouter);

//------------------------wrapper APIs------------------------------------------------------------||
app.use('/brokerFeature', brokerFeatureImport.brokerFeatureRouter);
app.use('/wrapperFeature', wrapperFeatureImport.wrapperFeatureRouter);
app.use('/brokerValidation', brokerValidationImport.brokerValidationeRouter);


app.use(function (req, res, next) { 
    next(createError(404)); 
}); 

app.use(function (err, req, res, next) { 
  
    // Set locals, only providing error 
    // in development 
    res.locals.message = err.message; 
    res.locals.error = req.app.get('env')  
            === 'development' ? err : {}; 
  
    // render the error page 
    res.status(err.status || 500); 
    res.send('error'); 
}); 

io.on("connection", socket => { console.log('... New client joined ...'); });

var integra = io.of('/integra');

    integra.on('connection', function(client) {
    console.log('someone connected');
    integra.emit('Status', 'New user joined');
    client.emit('Status', 'Connected !!!');

    client.on('send',(data)=>{
        client.emit('receive',data);
    });

    });

module.exports.socket = io;
require('./routes/wrapperEventRegistry');
console.log(io);
//integra.emit('connection');

//module.exports = { app: app, server: server, socket: io }; 
module.exports.app = app;
module.exports.server = server;
