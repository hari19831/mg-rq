'use strict';

var fs = require('fs');
var express = require('express');
var brokerFeature = express.Router();
var optionProcess = require('./brokerModule/config/common').optionProcess;
var defaultConfig = JSON.parse(fs.readFileSync(`./routes/brokerModule/config/brokerConfig.json`, `utf-8`)); //Fromserver
//----Module IMPORT---------||
var brokerFeatureMasterImport = require('./brokerModule/brokerFeatureMaster').brokerFeatureMaster;
// console.log(brokerFeatureMasterImport);
var featureSchemaImport = require('./brokerModule/brokerFeatureSchema');
var featureSchema = featureSchemaImport.brokerFeatureSchema;


brokerFeature.get('/', function (req, res, next) {
    res.send({ 'msg': '...Welcome to broker feature router...' });
});

brokerFeature.get('/methods', function (req, res, next) {
    res.setHeader('Content-Type', 'application/json');
    res.setTimeout(20000); //wait atleast for 20 secs

    getActions().then((msg) => {
        res.send(JSON.stringify(msg));
    }).catch((e) => {
        res.send(JSON.stringify(new featureSchema(false, true, false, [], 'getBrokerFeaturesMethods', 0, e.toString(), 'Null')));
    });

});

brokerFeature.post('/methods', function (req, res, next) {
    res.setHeader('Content-Type', 'application/json');
    res.setTimeout(20000); //wait atleast for 20 secs
    processActions(req.body).then((msg) => {
        res.send(JSON.stringify(msg));
    }).catch((e) => {
        res.send(JSON.stringify(new featureSchema(false, true, false, [], 'brokerFeature', 0, e.toString(), 'Null')));
    });
});

function getActions() {
    let start = new Date().getTime(); let end; let time;
    return new Promise((resolve, reject) => {
        try {
            let actionList = [];
            actionList = Object.keys(brokerFeatureMasterImport); //console.log(Object.keys(brokerFeatureMasterImport));
            end = new Date().getTime();
            time = end - start;
            resolve(new featureSchema(true, false, false, actionList, 'getBrokerFeaturesMethods', time, 'Null', 'Null')); //----->> SuccessEndPoint
        } catch (e) {
            end = new Date().getTime();
            time = end - start;
            console.log(e);
            resolve(new featureSchema(false, true, false, [], 'getBrokerFeaturesMethods', time, e.toString(), 'Null')); //----->> ErrorEndPoint
        }
    });
}

function processActions(body) {
    let start = new Date().getTime(); let end; let time;
    return new Promise(async (resolve, reject) => {
        try {
            let method = body.method;
            let productCode = body.productCode;
            let username = body.username;
            let password = body.password;
            let exchange = body.exchange;
            let queue = body.queue;
            let options = optionProcess(body);
            options = options == undefined ? {} : options;
            if (method == undefined || username == undefined || password == undefined || productCode == undefined) {
                end = new Date().getTime();
                time = end - start;
                resolve(new featureSchema(false, false, true, [], 'brokerFeature', time, 'Null', 'method, productCode, username and password inputs are required to process')); //----->> LimitaionEndPoint
            }
            let result = [];
            switch (method.toLowerCase()) {
                case 'aboutbroker':
                    result = await brokerFeatureMasterImport.aboutBroker(defaultConfig.admin.domain, username, password);
                    end = new Date().getTime();
                    time = end - start;
                    if (result.isSuccess) {
                        resolve(new featureSchema(true, false, false, result.result, 'brokerFeature', time, 'Null', 'Null')); //----->> SuccessEndPoint
                    } else {
                        result.type = 'brokerFeature';
                        result.time = time;
                        resolve(result); //----------->>FailureEndpoint
                    }
                    break;
                case 'aboutuser':
                    result = await brokerFeatureMasterImport.aboutUser(defaultConfig.admin.domain, username, password);
                    end = new Date().getTime();
                    time = end - start;
                    if (result.isSuccess) {
                        resolve(new featureSchema(true, false, false, result.result, 'brokerFeature', time, 'Null', 'Null')); //----->> SuccessEndPoint
                    } else {
                        result.type = 'brokerFeature';
                        result.time = time;
                        resolve(result); //----------->>FailureEndpoint
                    }
                    break;
                case 'listvhosts':
                    result = await brokerFeatureMasterImport.listVhosts(defaultConfig.admin.domain, username, password);
                    end = new Date().getTime();
                    time = end - start;
                    if (result.isSuccess) {
                        resolve(new featureSchema(true, false, false, result.result, 'brokerFeature', time, 'Null', 'Null')); //----->> SuccessEndPoint
                    } else {
                        result.type = 'brokerFeature';
                        result.time = time;
                        resolve(result); //----------->>FailureEndpoint
                    }
                    break;
                case 'listqueue':
                    result = await brokerFeatureMasterImport.listQueue(defaultConfig.admin.domain, productCode, username, password);
                    end = new Date().getTime();
                    time = end - start;
                    if (result.isSuccess) {
                        resolve(new featureSchema(true, false, false, result.result, 'brokerFeature', time, 'Null', 'Null')); //----->> SuccessEndPoint
                    } else {
                        result.type = 'brokerFeature';
                        result.time = time;
                        resolve(result); //----------->>FailureEndpoint
                    }
                    break;
                case 'isvhostlive':
                    result = await brokerFeatureMasterImport.isVhostLive(defaultConfig.admin.domain, productCode, username, password);
                    end = new Date().getTime();
                    time = end - start;
                    if (result.isSuccess) {
                        resolve(new featureSchema(true, false, false, result.result, 'brokerFeature', time, 'Null', 'Null')); //----->> SuccessEndPoint
                    } else {
                        result.type = 'brokerFeature';
                        result.time = time;
                        resolve(result); //----------->>FailureEndpoint
                    }
                    break;
                case 'listusers':
                    result = await brokerFeatureMasterImport.listUsers(defaultConfig.admin.domain, username, password);
                    end = new Date().getTime();
                    time = end - start;
                    if (result.isSuccess) {
                        resolve(new featureSchema(true, false, false, result.result, 'brokerFeature', time, 'Null', 'Null')); //----->> SuccessEndPoint
                    } else {
                        result.type = 'brokerFeature';
                        result.time = time;
                        resolve(result); //----------->>FailureEndpoint
                    }
                    break;
                case 'listconsumers':
                    result = await brokerFeatureMasterImport.listConsumers(defaultConfig.admin.domain, productCode, username, password);
                    end = new Date().getTime();
                    time = end - start;
                    if (result.isSuccess) {
                        resolve(new featureSchema(true, false, false, result.result, 'brokerFeature', time, 'Null', 'Null')); //----->> SuccessEndPoint
                    } else {
                        result.type = 'brokerFeature';
                        result.time = time;
                        resolve(result); //----------->>FailureEndpoint
                    }
                    break;
                case 'listexchanges':
                    result = await brokerFeatureMasterImport.listExchanges(defaultConfig.admin.domain, productCode, username, password);
                    end = new Date().getTime();
                    time = end - start;
                    if (result.isSuccess) {
                        resolve(new featureSchema(true, false, false, result.result, 'brokerFeature', time, 'Null', 'Null')); //----->> SuccessEndPoint
                    } else {
                        result.type = 'brokerFeature';
                        result.time = time;
                        resolve(result); //----------->>FailureEndpoint
                    }
                    break;
                case 'listqueuebindings':
                    result = await brokerFeatureMasterImport.listQueueBindings(defaultConfig.admin.domain, productCode, queue, username, password);
                    end = new Date().getTime();
                    time = end - start;
                    if (result.isSuccess) {
                        resolve(new featureSchema(true, false, false, result.result, 'brokerFeature', time, 'Null', 'Null')); //----->> SuccessEndPoint
                    } else {
                        result.type = 'brokerFeature';
                        result.time = time;
                        resolve(result); //----------->>FailureEndpoint
                    }
                    break;
                case 'listexchangebindingssource':
                    result = await brokerFeatureMasterImport.listExchangeBindingsSource(defaultConfig.admin.domain, productCode, exchange, username, password);
                    end = new Date().getTime();
                    time = end - start;
                    if (result.isSuccess) {
                        resolve(new featureSchema(true, false, false, result.result, 'brokerFeature', time, 'Null', 'Null')); //----->> SuccessEndPoint
                    } else {
                        result.type = 'brokerFeature';
                        result.time = time;
                        resolve(result); //----------->>FailureEndpoint
                    }
                    break;
                case 'listexchangebindingsdestination':
                    result = await brokerFeatureMasterImport.listExchangeBindingsDestination(defaultConfig.admin.domain, productCode, exchange, username, password);
                    end = new Date().getTime();
                    time = end - start;
                    if (result.isSuccess) {
                        resolve(new featureSchema(true, false, false, result.result, 'brokerFeature', time, 'Null', 'Null')); //----->> SuccessEndPoint
                    } else {
                        result.type = 'brokerFeature';
                        result.time = time;
                        resolve(result); //----------->>FailureEndpoint
                    }
                    break;
                case 'bindexchangetoqueue':
                    result = await brokerFeatureMasterImport.bindExchangeToQueue(defaultConfig.admin.domain, username, password, productCode, exchange, queue, options);
                    end = new Date().getTime();
                    time = end - start;
                    if (result.isSuccess) {
                        resolve(new featureSchema(true, false, false, result.result, 'brokerFeature', time, 'Null', 'Null')); //----->> SuccessEndPoint
                    } else {
                        result.type = 'brokerFeature';
                        result.time = time;
                        resolve(result); //----------->>FailureEndpoint
                    }
                    break;
                case 'createexchange':
                    result = await brokerFeatureMasterImport.createExchange(defaultConfig.admin.domain, username, password, productCode, exchange, options);
                    end = new Date().getTime();
                    time = end - start;
                    if (result.isSuccess) {
                        resolve(new featureSchema(true, false, false, result.result, 'brokerFeature', time, 'Null', 'Null')); //----->> SuccessEndPoint
                    } else {
                        result.type = 'brokerFeature';
                        result.time = time;
                        resolve(result); //----------->>FailureEndpoint
                    }
                    break;
                case 'createqueue':
                    result = await brokerFeatureMasterImport.createQueue(defaultConfig.admin.domain, username, password, productCode, queue, options);
                    end = new Date().getTime();
                    time = end - start;
                    if (result.isSuccess) {
                        resolve(new featureSchema(true, false, false, result.result, 'brokerFeature', time, 'Null', 'Null')); //----->> SuccessEndPoint
                    } else {
                        result.type = 'brokerFeature';
                        result.time = time;
                        resolve(result); //----------->>FailureEndpoint
                    }
                    break;
                case 'createuser':
                    let user = options.username;
                    delete options.username;
                    result = await brokerFeatureMasterImport.createUser(defaultConfig.admin.domain, defaultConfig.admin.user, defaultConfig.admin.password, user, options);
                    end = new Date().getTime();
                    time = end - start;
                    if (result.isSuccess) {
                        resolve(new featureSchema(true, false, false, result.result, 'brokerFeature', time, 'Null', 'Null')); //----->> SuccessEndPoint
                    } else {
                        result.type = 'brokerFeature';
                        result.time = time;
                        resolve(result); //----------->>FailureEndpoint
                    }
                    break;
                case 'createvhost':
                    result = await brokerFeatureMasterImport.createVhost(defaultConfig.admin.domain, username, password, productCode, options);
                    end = new Date().getTime();
                    time = end - start;
                    if (result.isSuccess) {
                        resolve(new featureSchema(true, false, false, result.result, 'brokerFeature', time, 'Null', 'Null')); //----->> SuccessEndPoint
                    } else {
                        result.type = 'brokerFeature';
                        result.time = time;
                        resolve(result); //----------->>FailureEndpoint
                    }
                    break;
                case 'updateuservhostpermission':
                    result = await brokerFeatureMasterImport.updateUserVhostPermission(defaultConfig.admin.domain, defaultConfig.admin.user, defaultConfig.admin.password, productCode, username, options);
                    end = new Date().getTime();
                    time = end - start;
                    if (result.isSuccess) {
                        resolve(new featureSchema(true, false, false, result.result, 'brokerFeature', time, 'Null', 'Null')); //----->> SuccessEndPoint
                    } else {
                        result.type = 'brokerFeature';
                        result.time = time;
                        resolve(result); //----------->>FailureEndpoint
                    }
                    break;
                case 'deleteexchange':
                    result = await brokerFeatureMasterImport.deleteExchange(defaultConfig.admin.domain, username, password, productCode, exchange);
                    end = new Date().getTime();
                    time = end - start;
                    if (result.isSuccess) {
                        resolve(new featureSchema(true, false, false, result.result, 'brokerFeature', time, 'Null', 'Null')); //----->> SuccessEndPoint
                    } else {
                        result.type = 'brokerFeature';
                        result.time = time;
                        resolve(result); //----------->>FailureEndpoint
                    }
                    break;
                case 'deletequeue':
                    result = await brokerFeatureMasterImport.deleteQueue(defaultConfig.admin.domain, username, password, productCode, queue, options);
                    end = new Date().getTime();
                    time = end - start;
                    if (result.isSuccess) {
                        resolve(new featureSchema(true, false, false, result.result, 'brokerFeature', time, 'Null', 'Null')); //----->> SuccessEndPoint
                    } else {
                        result.type = 'brokerFeature';
                        result.time = time;
                        resolve(result); //----------->>FailureEndpoint
                    }
                    break;
                case 'deletequeuemessage':
                    result = await brokerFeatureMasterImport.deleteQueueMessage(defaultConfig.admin.domain, username, password, productCode, queue);
                    end = new Date().getTime();
                    time = end - start;
                    if (result.isSuccess) {
                        resolve(new featureSchema(true, false, false, result.result, 'brokerFeature', time, 'Null', 'Null')); //----->> SuccessEndPoint
                    } else {
                        result.type = 'brokerFeature';
                        result.time = time;
                        resolve(result); //----------->>FailureEndpoint
                    }
                    break;
                case 'deleteuser':
                    result = await brokerFeatureMasterImport.deleteUser(defaultConfig.admin.domain, defaultConfig.admin.user, defaultConfig.admin.password, username);
                    end = new Date().getTime();
                    time = end - start;
                    if (result.isSuccess) {
                        resolve(new featureSchema(true, false, false, result.result, 'brokerFeature', time, 'Null', 'Null')); //----->> SuccessEndPoint
                    } else {
                        result.type = 'brokerFeature';
                        result.time = time;
                        resolve(result); //----------->>FailureEndpoint
                    }
                    break;
                case 'deleteuservhostpermission':
                    result = await brokerFeatureMasterImport.deleteUserVhostPermission(defaultConfig.admin.domain, defaultConfig.admin.user, defaultConfig.admin.password, productCode, username);
                    end = new Date().getTime();
                    time = end - start;
                    if (result.isSuccess) {
                        resolve(new featureSchema(true, false, false, result.result, 'brokerFeature', time, 'Null', 'Null')); //----->> SuccessEndPoint
                    } else {
                        result.type = 'brokerFeature';
                        result.time = time;
                        resolve(result); //----------->>FailureEndpoint
                    }
                    break;
                case 'deletevhost':
                    result = await brokerFeatureMasterImport.deleteVhost(defaultConfig.admin.domain, username, password, productCode);
                    end = new Date().getTime();
                    time = end - start;
                    if (result.isSuccess) {
                        resolve(new featureSchema(true, false, false, result.result, 'brokerFeature', time, 'Null', 'Null')); //----->> SuccessEndPoint
                    } else {
                        result.type = 'brokerFeature';
                        result.time = time;
                        resolve(result); //----------->>FailureEndpoint
                    }
                    break;
                case 'updatequeueaction':
                    result = await brokerFeatureMasterImport.updateQueueAction(defaultConfig.admin.domain, username, password, productCode, queue, options);
                    end = new Date().getTime();
                    time = end - start;
                    if (result.isSuccess) {
                        resolve(new featureSchema(true, false, false, result.result, 'brokerFeature', time, 'Null', 'Null')); //----->> SuccessEndPoint
                    } else {
                        result.type = 'brokerFeature';
                        result.time = time;
                        resolve(result); //----------->>FailureEndpoint
                    }
                    break;
                case 'consumemessage':
                    result = await brokerFeatureMasterImport.consumeMessage(defaultConfig.admin.domain, username, password, productCode, queue, options);
                    end = new Date().getTime();
                    time = end - start;
                    if (result.isSuccess) {
                        resolve(new featureSchema(true, false, false, result.result, 'brokerFeature', time, 'Null', 'Null')); //----->> SuccessEndPoint
                    } else {
                        result.type = 'brokerFeature';
                        result.time = time;
                        resolve(result); //----------->>FailureEndpoint
                    }
                    break;
                case 'publishmessage':
                    result = await brokerFeatureMasterImport.publishMessage(defaultConfig.admin.domain, username, password, productCode, exchange, options);
                    end = new Date().getTime();
                    time = end - start;
                    if (result.isSuccess) {
                        resolve(new featureSchema(true, false, false, result.result, 'brokerFeature', time, 'Null', 'Null')); //----->> SuccessEndPoint
                    } else {
                        result.type = 'brokerFeature';
                        result.time = time;
                        resolve(result); //----------->>FailureEndpoint
                    }
                    break;
                default:
                    end = new Date().getTime();
                    time = end - start;
                    resolve(new featureSchema(false, false, true, [], 'brokerFeature', time, 'Null', 'Please input a valid action to perform')); //----->> LimitaionEndPoint
            }
        } catch (e) {
            end = new Date().getTime();
            time = end - start;
            console.log(e);
            resolve(new featureSchema(false, true, false, [], 'brokerFeature', time, e.toString(), 'Null')); //----->> ErrorEndPoint
        }
    });
}

module.exports.brokerFeatureRouter = brokerFeature;
module.exports.processActions_brokerFeature = processActions;
