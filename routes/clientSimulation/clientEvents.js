'use strict';

const io = require("socket.io-client");
var ioClient = io.connect("http://localhost:7979/inlp", { auth: { token: '1feeda71824578b1b3e8e146582247846d704269b3fbc986704d44511d237a45be5b6cd2dbc825d9e9e443ad520945fda0cbb794c0892c89c91f72944befafba12fa1dbc86ee7971b2c64377c59dc1cc2ad40d80186ee9cd3da472c3997726b0d4bc10d5282b503692ef2edf79bbde3bdb4d44a7c0bc1a58d8046c883fff8403842e54bdf7cecc799c884bbadaa1e041322cf10e0e8f37a89f50a509fd20c26128' } });

ioClient.on("connect", (socket) => {
    console.log(ioClient.id);
});

ioClient.on("receive", (socket) => {
    console.log(socket);
});

ioClient.on("connect_error", (socket) => {
    console.log(socket.message);
});

ioClient.on("disconnect", (socket) => {
    console.log("I am disconnected");
});

//list the methods available
ioClient.emit("wrapperMethods", {})
ioClient.on("wrapperMethodsResponse", (data) => {
    console.log(data);
})

//createQueue
ioClient.emit("wrapperEvent", {
    method: "createQueue",
    activity: "create",
    queue: "test"
})

//createexchange
ioClient.emit("wrapperEvent", {
    method: "createexchange",
    activity: "create",
    exchange: "test"
})

//bindexchangetoqueue
ioClient.emit("wrapperEvent", {
    method: "bindexchangetoqueue",
    activity: "create",
    exchange: "test",
    queue: "test"
})

//updatequeueaction
ioClient.emit("wrapperEvent", {
    method: "updatequeueaction",
    activity: "create",
    queue: "test",
    action: "sync"
})

//publishMessage
ioClient.emit("wrapperEvent", {
    method: "publishMessage",
    activity: "create",
    action: "sync",
    payload_encoding: "string",
    payload: "tesing string",
    exchange: "test",
    properties: {}
})

//consumemessage
ioClient.emit("wrapperEvent", {
    method: "consumemessage",
    count: 1,
    ackmode: "ack_requeue_true",
    encoding: "auto",
    truncate: 50000,
    queue: "test",
    activity: "read"
})

//listconsumers
ioClient.emit("wrapperEvent", {
    method: "listconsumers",
    activity: "read",
    queue: "test"
})

//listqueue
ioClient.emit("wrapperEvent", {
    method: "listqueue",
    activity: "read"
})

//listExchanges
ioClient.emit("wrapperEvent", {
    method: "listExchanges",
    activity: "read"
})

//listQueueBindings
ioClient.emit("wrapperEvent", {
    method: "listQueueBindings",
    activity: "read",
    queue: "test"
})

//listExchangeBindingsSource
ioClient.emit("wrapperEvent", {
    method: "listExchangeBindingsSource",
    activity: "read",
    exchange: "test"
})

//listExchangeBindingsDestination
ioClient.emit("wrapperEvent", {
    method: "listExchangeBindingsDestination",
    activity: "read",
    exchange: "test"
})

//deleteQueueMessage
ioClient.emit("wrapperEvent", {
    method: "deleteQueueMessage",
    activity: "delete",
    queue: "test"
})

//deletequeue
ioClient.emit("wrapperEvent", {
    method: "deletequeue",
    activity: "delete",
    queue: "test"
})

//deleteExchange
ioClient.emit("wrapperEvent", {
    method: "deleteExchange",
    activity: "delete",
    exchange: "test"
})

ioClient.on("wrapperEventResponse", (data) => {
    console.log(data);
})

