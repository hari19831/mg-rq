'use strict';

var eventEmitter = require('events');
var productEventMaster = {};

//-----Module-Imports-----||
var io = require('../app').socket;
var productsConfig = require('../products.json');
var WrapperFeature = require('./wrapperFeatureRouter');
var featureSchemaImport = require('./brokerModule/brokerFeatureSchema');
var featureSchema = featureSchemaImport.brokerFeatureSchema;
var keyVault = require("../ll/vaultActions_method");

function dynamicEventRegistry() {
    return new Promise(async (resolve, reject) => {
        try {
            if (productsConfig.db) {
                let productList = Object.keys(productsConfig.db) || [];
                console.log(productList);
                productList.forEach((v) => {
                    ProductEvent(v);
                });
                resolve(productEventMaster);
            }
        } catch (e) {
            console.log(e);
        }
    });
}

function ProductEvent(v) {
    productEventMaster[v] = io.of(`/${v}`);
    productEventMaster[v].use(async function (client, next) {
        if (client.handshake.auth.token) {
            let token  = await keyVault.getSecret(productsConfig.db[v].token);
            if (client.handshake.auth.token != token) {
                next(new Error('Invalid Auth token.'));
                //notification mailer upcoming update
            }
            else {
                next();
            }
        } else {
            next(new Error('Auth token missing.'));
        }
    }).on('connection', function (client) {
        productEventMaster[v].emit('Status', 'New user joined');
        client.emit('receive', 'Connected !!!');
        client.on(`wrapperMethods`, (data) => {
            WrapperFeature.getActions().then(result => {
                client.emit(`wrapperMethodsResponse`, result);
            })
        });
        client.on(`wrapperEvent`, (data) => {
            let start = new Date().getTime(); let end; let time;
            try {
                end = new Date().getTime();
                time = end - start;
                data.token = client.handshake.auth.token;
                data.productCode = v;
                WrapperFeature.processActions(data).then(result => {
                    result.method = data.method;
                    client.emit(`wrapperEventResponse`, result);
                })
            } catch (error) {
                client.emit(`wrapperEventResponse`, JSON.stringify(new featureSchema(false, true, false, [], 'validateAction', 0, e.toString(), 'Null')));
            }
        });
    });
}

function dynamicEvents(productCode, authenticationToken) {
    return new Promise((resolve, reject) => {

    });
}

dynamicEventRegistry().then((msg) => {
    //console.log(msg);
    //console.log(io);
    console.log("dynamicEventRegistry Triggured.")
})

module.exports.ProductEvent = ProductEvent;

