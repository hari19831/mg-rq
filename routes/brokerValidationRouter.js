'use strict';

var fs = require('fs');
var express = require('express');
var brokerValidation = express.Router();

//----Module IMPORT---------||
var brokerValidationMasterImport = require('./brokerModule/validation/brokerValidationMaster').brokerValidationMaster;
var featureSchemaImport = require('./brokerModule/brokerFeatureSchema');
var featureSchema = featureSchemaImport.brokerFeatureSchema;


brokerValidation.get('/', function(req, res, next) {
  res.send({'msg':'...Welcome to broker validation router...'});
});

brokerValidation.get('/actions', function(req, res, next) {
    res.setHeader('Content-Type','application/json');
    res.setTimeout(20000); //wait atleast for 20 secs
    
    getActions().then((msg)=>{
        res.send(JSON.stringify(msg));
    }).catch((e)=>{
        res.send(JSON.stringify(new featureSchema(false, true, false, [], 'getValidationAction', 0, e.toString(), 'Null')));
    });
    
});

brokerValidation.post('/actions', function(req, res, next) {
    res.setHeader('Content-Type','application/json');
    res.setTimeout(20000); //wait atleast for 20 secs
    
    let actionName = req.body.action;
    let productCode = req.body.productCode;
    let item = req.body.item;

    processActions(actionName, productCode, item).then((msg)=>{
        res.send(JSON.stringify(msg));
    }).catch((e)=>{
        res.send(JSON.stringify(new featureSchema(false, true, false, [], 'validateAction', 0, e.toString(), 'Null')));
    }); 
});

function getActions(){
    let start = new Date().getTime(); let end; let time;
    return new Promise((resolve, reject)=>{
        try{
            let actionList = [];
                actionList = Object.keys(brokerValidationMasterImport); //console.log(Object.keys(brokerValidationMasterImport));
            end = new Date().getTime();
            time = end - start;
            resolve(new featureSchema(true, false, false, actionList, 'getValidationAction', time, 'Null', 'Null')); //----->> SuccessEndPoint
        }catch(e){
            end = new Date().getTime();
            time = end - start;
            console.log(e);
            resolve(new featureSchema(false, true, false, [], 'getValidationAction', time, e.toString(), 'Null')); //----->> ErrorEndPoint
        }
    });
}

function processActions(action, productCode, item){
    let start = new Date().getTime(); let end; let time;
    return new Promise(async(resolve, reject)=>{
        try{
            if(action == undefined || item == undefined){
                end = new Date().getTime();
                time = end - start;
                resolve(new featureSchema(false, false, true, [], 'validateAction', time, 'Null', 'Action & Item inputs are required to process')); //----->> LimitaionEndPoint
            }
            let result = [];
            switch(action.toLowerCase()){
                case 'validateuser':
                    result = await brokerValidationMasterImport.validateUser(item);
                    end = new Date().getTime();
                    time = end - start;
                    if(result.isSuccess){
                        resolve(new featureSchema(true, false, false, result.result, 'validateUser', time, 'Null', 'Null')); //----->> SuccessEndPoint
                    }else{
                        result.type = 'validateUser';
                        result.time = time;
                        resolve(result); //----------->>FailureEndpoint
                    }
                    break;
                case 'validatevhost':
                    result = await brokerValidationMasterImport.validateVhost(item);
                    end = new Date().getTime();
                    time = end - start;
                    if(result.isSuccess){
                        resolve(new featureSchema(true, false, false, result.result, 'validateVhost', time, 'Null', 'Null')); //----->> SuccessEndPoint
                    }else{
                        result.type = 'validateVhost';
                        result.time = time;
                        resolve(result);
                    }
                    break;
                case 'validatequeue':
                    result = await brokerValidationMasterImport.validateQueue(productCode, item);
                    end = new Date().getTime();
                    time = end - start;
                    if(result.isSuccess){
                        resolve(new featureSchema(true, false, false, result.result, 'validateQueue', time, 'Null', 'Null')); //----->> SuccessEndPoint
                    }else{
                        result.type = 'validateQueue';
                        result.time = time;
                        resolve(result);
                    }
                    break;
                case 'validateexchange':
                    result = await brokerValidationMasterImport.validateExchange(productCode, item);
                    end = new Date().getTime();
                    time = end - start;
                    if(result.isSuccess){
                        resolve(new featureSchema(true, false, false, result.result, 'validateExchange', time, 'Null', 'Null')); //----->> SuccessEndPoint
                    }else{
                        result.type = 'validateExchange';
                        result.time = time;
                        resolve(result);
                    }
                    break;
                default:
                    end = new Date().getTime();
                    time = end - start;
                    resolve(new featureSchema(false, false, true, [], 'validateAction', time, 'Null', 'Please input a valid action to perform')); //----->> LimitaionEndPoint
            }            
        }catch(e){
            end = new Date().getTime();
            time = end - start;
            console.log(e);
            resolve(new featureSchema(false, true, false, [], 'validateAction', time, e.toString(), 'Null')); //----->> ErrorEndPoint
        }
    });
}


module.exports.brokerValidationeRouter = brokerValidation;