
var axios = require('axios');
var btoa = require('btoa');


//---Imports---||
var featureSchemaImport = require('./brokerFeatureSchema');
var featureSchema = featureSchemaImport.brokerFeatureSchema;

function UpdateUservHostPermission(domain, u_name, u_password, vhost, user, options) { //---API Endpoint - /api/permissions/${vhost}/${user} [PUT]
    let start = new Date().getTime(); let end; let time;
    return new Promise(async (resolve, reject) => {
        try {
                // let userList = await listUsers(domain, u_name, u_password);
                // if (userList.isError == true) { throw userList }
                // let users = [];
                // userList.result.forEach(element => {
                //     users.push(element.name)
                // });
                // if (!users.includes(user)) {
                //     end = new Date().getTime();
                //     time = end - start;
                //     resolve(new featureSchema(false, false, [], 'putUservHostPermission', time, null, true, "No such user available."));
                //     return;
                // }
                // let vhostList = await listVhosts(domain, u_name, u_password);
                // if (vhostList.isError == true) { throw vhostList }
                // let vhosts = [];
                // vhostList.result.forEach(element => {
                //     vhosts.push(element.name)
                // });
                // if (!vhosts.includes(vhost)) {
                //     end = new Date().getTime();
                //     time = end - start;
                //     resolve(new featureSchema(false, false, [], 'putUservHostPermission', time, null, true, "No such vhost available."));
                //     return;
                // }
                if (domain == undefined || u_name == undefined || u_password == undefined || vhost == undefined || user == undefined) {
                    end = new Date().getTime();
                    time = end - start;
                    resolve(new featureSchema(false, false, true, [], 'UpdateUservHostPermission', time, 'Null', `one or more parameters required is missing`)); //----->> LimitationEndPoint
                }
            
                let encodedPass = btoa(`${u_name}:${u_password}`) || '';
                let config = {
                    method: 'put',
                    url: `http://${domain}/api/permissions/${vhost}/${user}`,
                    headers: {
                        'Authorization': `Basic ${encodedPass}`,
                        'Content-Type': 'application/json'
                    },
                    data: options
                };
                axios(config).then(function (response) {
                    end = new Date().getTime();
                    time = end - start;
                    resolve(new featureSchema(true, false, false, response.data, 'updateUservHostPermission', time, 'Null', 'Null')); //----->> SuccessEndPoint
                }).catch(function (err2) {
                    let resp = (err2.response == undefined) ? [] : err2.response.data;
                    end = new Date().getTime();
                    time = end - start;
                    resolve(new featureSchema(false, true, false, resp, 'updateUservHostPermission', time, err2.toString(), 'Null')); //----->> ErrorEndPoint
                });
        } catch (err1) {
            end = new Date().getTime();
            time = end - start;
            console.log(err1);
            resolve(new featureSchema(false, true, false, [], 'updateUservHostPermission', time, err1.toString(), 'Null')); //----->> ErrorEndPoint
        }
    });
}
// putUservHostPermission('issez-s155:15672','test','test','iAuthor','test1',{"configure":".*","write":".*","read":".*"}).then(x=>{
//     console.log(x);
// }).catch(e=>{
//     console.log(e);
// })

module.exports.UpdateUservHostPermission = UpdateUservHostPermission;