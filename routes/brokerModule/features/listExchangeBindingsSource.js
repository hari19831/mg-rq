'use strict';

var axios = require('axios');
var btoa = require('btoa');

//---Imports---||
var featureSchemaImport = require('./brokerFeatureSchema');
var featureSchema = featureSchemaImport.brokerFeatureSchema;

function listExchangeBindingsSource(domain, vhost, exchange, u_name, u_password) { //---API Endpoint - /api/exchanges/${vhost}/${exchange}/bindings/source [GET]

    let start = new Date().getTime(); let end; let time;

    return new Promise((resolve, reject) => {
        try {

            if(domain == undefined || vhost == undefined || exchange == undefined || u_name == undefined || u_password == undefined){
                end = new Date().getTime();
                time = end - start;
                resolve(new featureSchema(false, false, true, [], 'listExchangeBindingsSource', time, 'Null', `one or more parameters required is missing`)); //----->> LimitationEndPoint
            }
            
            let encodedPass = btoa(`${u_name}:${u_password}`) || '';
                let config = {
                    method: 'get',
                    // url: 'http://localhost:15672/api/exchanges/harry_host/harry_fanout/bindings/source',
                    url: `http://${domain}/api/exchanges/${vhost}/${exchange}/bindings/source`,
                    headers: {
                        'Authorization': `Basic ${encodedPass}`
                    }
                };

                axios(config).then(function (response) {
                    //console.log(JSON.stringify(response.data));
                    end = new Date().getTime();
                    time = end - start;
                    resolve(new featureSchema(true, false, false, response.data, 'listExchangeBindingsSource', time, 'Null')); //----->> SuccessEndPoint
                }).catch(function (err2) {
                    //console.log(Object.keys(err2));
                    let resp = (err2.response == undefined) ? [] : err2.response.data;
                    end = new Date().getTime();
                    time = end - start;
                    resolve(new featureSchema(false, true, false, resp, 'listExchangeBindingsSource', time, err2.toString(), 'Null')); //----->> ErrorEndPoint
                });

        } catch (err1) {
            end = new Date().getTime();
            time = end - start;
            console.log(err1);
            resolve(new featureSchema(false, true, false, [], 'listExchangeBindingsSource', time, err1.toString(), 'Null')); //----->> ErrorEndPoint
        }
    });

}
//listExchangeBindingsSource('localhost:15672', 'harry_host', 'harry_direct', 'guest', 'guest').then((msg)=>{ console.log(msg); });

module.exports.listExchangeBindingsSource = listExchangeBindingsSource;