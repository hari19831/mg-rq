var axios = require('axios');
var btoa = require('btoa');


//---Imports---||
var featureSchemaImport = require('./brokerFeatureSchema');
var featureSchema = featureSchemaImport.brokerFeatureSchema;

function publish(domain, u_name, u_password, vhost, exchange, options) { //---API Endpoint - /api/exchanges/${vhost}/${exchange}/publish [POST]
    let start = new Date().getTime(); let end; let time;
    return new Promise(async (resolve, reject) => {
        try {
				if (domain == undefined || u_name == undefined || u_password == undefined || vhost == undefined || exchange == undefined || options == undefined) {
					end = new Date().getTime();
					time = end - start;
					resolve(new featureSchema(false, false, true, [], 'publish', time, 'Null', `one or more parameters required is missing`)); //----->> LimitationEndPoint
				}
				
				let encodedPass = btoa(`${u_name}:${u_password}`) || '';
				let config = {
					method: 'post',
					url: `http://${domain}/api/exchanges/${vhost}/${exchange}/publish`,
					headers: {
						'Authorization': `Basic ${encodedPass}`,
						'Content-Type': 'application/json'
					},
					data: options
				};
				axios(config).then(function (response) {
					end = new Date().getTime();
					time = end - start;
					resolve(new featureSchema(true, false, false, response.data, 'publish', time, 'Null', 'Null')); //----->> SuccessEndPoint
				}).catch(function (err2) {
					let resp = (err2.response == undefined) ? [] : err2.response.data;
					end = new Date().getTime();
					time = end - start;
					resolve(new featureSchema(false, true, false, resp, 'publish', time, err2.toString(), 'Null')); //----->> ErrorEndPoint
				});
        } catch (err1) {
            end = new Date().getTime();
            time = end - start;
            console.log(err1);
            resolve(new featureSchema(false, true, false, [], 'publish', time, err1.toString(), 'Null')); //----->> ErrorEndPoint
        }
    });
}
// publish('issez-s155:15672','test','test','iAuthor','iAuthor',{"properties":{},"routing_key":"my_routing_key","payload":"my body","payload_encoding":"string"}).then(x=>{
//     console.log(x);
// }).catch(e=>{
//     console.log(e);
// })

module.exports.publish = publish;