
var axios = require('axios');
var btoa = require('btoa');

//---Imports---||
var featureSchemaImport = require('./brokerFeatureSchema');
var featureSchema = featureSchemaImport.brokerFeatureSchema;

function deleteQueueData(domain, u_name, u_password, vhost, queue) { //---API Endpoint - /api/queues/${vhost}/${queue}/contents [DELETE]
    let start = new Date().getTime(); let end; let time;
    return new Promise(async (resolve, reject) => {
        try {
                if (domain == undefined || u_name == undefined || u_password == undefined || vhost == undefined || queue == undefined) {
                    end = new Date().getTime();
                    time = end - start;
                    resolve(new featureSchema(false, false, true, [], 'deleteQueueData', time, 'Null', `one or more parameters required is missing`)); //----->> LimitationEndPoint
                }
            
                let encodedPass = btoa(`${u_name}:${u_password}`) || '';
                let config = {
                    method: 'delete',
                    url: `http://${domain}/api/queues/${vhost}/${queue}/contents`,
                    headers: {
                        'Authorization': `Basic ${encodedPass}`,
                        'Content-Type': 'application/json'
                    }
                };
                axios(config).then(function (response) {
                    end = new Date().getTime();
                    time = end - start;
                    resolve(new featureSchema(true, false, false, response.data, 'deleteQueueData', time, 'Null', 'Null')); //----->> SuccessEndPoint
                }).catch(function (err2) {
                    let resp = (err2.response == undefined) ? [] : err2.response.data;
                    end = new Date().getTime();
                    time = end - start;
                    resolve(new featureSchema(false, true, false, resp, 'deleteQueueData', time, err2.toString(), 'Null')); //----->> ErrorEndPoint
                });
        } catch (err1) {
            end = new Date().getTime();
            time = end - start;
            console.log(err1);
            resolve(new featureSchema(false, true, false, [], 'deleteQueueData', time, err1.toString(), 'Null')); //----->> ErrorEndPoint
        }
    });
}
//deleteQueueData('localhost:15672','guest','guest','harry_host','engine_2_publish').then(x=>{ console.log(x); });

module.exports.deleteQueueData = deleteQueueData;