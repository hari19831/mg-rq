'use strict';

var axios = require('axios');
var btoa  = require('btoa');

// var http = require('http');
// var http_Agent = require('agentkeepalive');

// var https = require('https');
// var https_Agent = require('agentkeepalive').HttpsAgent;

// var keepaliveAgent_http = new http_Agent({
//                                             maxSockets: 100,
//                                             maxFreeSockets: 10,
//                                             timeout: 60000, // active socket keepalive for 60 seconds
//                                             freeSocketTimeout: 60000, // free socket keepalive for 60 seconds
//                                         });

// var keepaliveAgent_https = new http_Agent({
//                                             maxSockets: 100,
//                                             maxFreeSockets: 10,
//                                             timeout: 60000, // active socket keepalive for 60 seconds
//                                             freeSocketTimeout: 60000, // free socket keepalive for 60 seconds
//                                         });




//---Imports---||
//var validationImport = require('./../validation/brokerValidationMaster').brokerValidationMaster;
var featureSchemaImport = require('./brokerFeatureSchema');
var featureSchema = featureSchemaImport.brokerFeatureSchema;

function aboutBroker(domain, u_name, u_password) { //---API Endpoint - /api/overview [GET]

    let start = new Date().getTime(); let end; let time;

    return new Promise((resolve, reject) => {
        try {

                if(domain == undefined || u_name == undefined || u_password == undefined){
                    end = new Date().getTime();
                    time = end - start;
                    resolve(new featureSchema(false, false, true, [], 'aboutBroker', time, 'Null', `one or more parameters required is missing`)); //----->> LimitationEndPoint
                }

                let encodedPass = btoa(`${u_name}:${u_password}`) || '';
                let config = {
                    method: 'get',
                    // url: 'http://localhost:15672/api/overview',
                    url: `http://${domain}/api/overview/`,
                    headers: {
                        'Authorization': `Basic ${encodedPass}`
                    }
                };

                //config.agent = keepaliveAgent_http;

                axios(config).then(function (response) {
                    //console.log(response);
                    //console.log(JSON.stringify(response.data));
                    end = new Date().getTime();
                    time = end - start;
                    resolve(new featureSchema(true, false, false, response.data, 'aboutBroker', time, 'Null', 'Null')); //----->> SuccessEndPoint
                }).catch(function (err2) {
                    //console.log(Object.keys(err2)); 
                    console.log(err2);
                    let resp = (err2.response == undefined) ? [] : err2.response.data;
                    end = new Date().getTime();
                    time = end - start;
                    resolve(new featureSchema(false, true, false, resp, 'aboutBroker', time, err2.toString(), 'Null')); //----->> ErrorEndPoint
                });

        } catch (err1) {
            end = new Date().getTime();
            time = end - start;
            console.log(err1);
            resolve(new featureSchema(false, true, false, [], 'aboutBroker', time, err1.toString(), 'Null')); //----->> ErrorEndPoint
        }
    });

}
//aboutBroker('localhost:15672','guest', 'guest').then((msg)=>{ console.log(msg); }); 

module.exports.aboutBroker = aboutBroker;