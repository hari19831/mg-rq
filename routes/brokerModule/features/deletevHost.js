var axios = require('axios');
var btoa = require('btoa');


//---Imports---||
var featureSchemaImport = require('./brokerFeatureSchema');
var featureSchema = featureSchemaImport.brokerFeatureSchema;

function deletevHost(domain, u_name, u_password, vhost) { //---API Endpoint - /api/vhosts/${vhost} [DELETE]
    let start = new Date().getTime(); let end; let time;
    return new Promise(async (resolve, reject) => {
        try {
				if (domain == undefined || u_name == undefined || u_password == undefined || vhost == undefined) {
					end = new Date().getTime();
					time = end - start;
					resolve(new featureSchema(false, false, true, [], 'deletevHost', time, 'Null', `one or more parameters required is missing`)); //----->> LimitationEndPoint
				}
				
				let encodedPass = btoa(`${u_name}:${u_password}`) || '';
				let config = {
					method: 'delete',
					url: `http://${domain}/api/vhosts/${vhost}`,
					headers: {
						'Authorization': `Basic ${encodedPass}`,
						'Content-Type': 'application/json'
					}
				};
				axios(config).then(function (response) {
					end = new Date().getTime();
					time = end - start;
					resolve(new featureSchema(true, false, false, response.data, 'deletevHost', time, 'Null', 'Null')); //----->> SuccessEndPoint
				}).catch(function (err2) {
					let resp = (err2.response == undefined) ? [] : err2.response.data;
					end = new Date().getTime();
					time = end - start;
					resolve(new featureSchema(false, true, false, resp, 'deletevHost', time, err2.toString(), 'Null')); //----->> ErrorEndPoint
				});
        } catch (err1) {
            end = new Date().getTime();
            time = end - start;
            console.log(err1);
            resolve(new featureSchema(false, true, false, [], 'deletevHost', time, err1.toString(), 'Null')); //----->> ErrorEndPoint
        }
    });
}
// deletevHost('issez-s155:15672','test','test','iAuthor').then(x=>{
//     console.log(x);
// }).catch(e=>{
//     console.log(e);
// })

module.exports.deletevHost = deletevHost;