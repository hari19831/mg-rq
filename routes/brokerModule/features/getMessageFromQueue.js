var axios = require('axios');
var btoa = require('btoa');


//---Imports---||
var featureSchemaImport = require('./brokerFeatureSchema');
var featureSchema = featureSchemaImport.brokerFeatureSchema;


function getMessageFromQueue(domain, u_name, u_password, vhost, queue, options) { //---API Endpoint - /api/${queues}/${vhost}/name/get [POST]
    let start = new Date().getTime(); let end; let time;
    return new Promise(async (resolve, reject) => {
        try {
                if (domain == undefined || u_name == undefined || u_password == undefined || vhost == undefined || queue == undefined || options == undefined) {
                    end = new Date().getTime();
                    time = end - start;
                    resolve(new featureSchema(false, false, true, [], 'getMessageFromQueue', time, 'Null', `one or more parameters required is missing`)); //----->> LimitationEndPoint
                }
                
                let encodedPass = btoa(`${u_name}:${u_password}`) || '';
                let config = {
                    method: 'post',
                    url: `http://${domain}/api/queues/${vhost}/${queue}/get`,
                    headers: {
                        'Authorization': `Basic ${encodedPass}`,
                        'Content-Type': 'application/json'
                    },
                    data: options
                };
                axios(config).then(function (response) {
                    end = new Date().getTime();
                    time = end - start;
                    resolve(new featureSchema(true, false, false, response.data, 'getMessageFromQueue', time, 'Null', 'Null')); //----->> SuccessEndPoint
                }).catch(function (err2) {
                    let resp = (err2.response == undefined) ? [] : err2.response.data;
                    end = new Date().getTime();
                    time = end - start;
                    resolve(new featureSchema(false, true, false, resp, 'getMessageFromQueue', time, err2.toString(), 'Null')); //----->> ErrorEndPoint
                });
        } catch (err1) {
            end = new Date().getTime();
            time = end - start;
            console.log(err1);
            resolve(new featureSchema(false, true, false, [], 'getMessageFromQueue', time, err1.toString(), 'Null')); //----->> ErrorEndPoint
        }
    });
}
// getMessageFromQueue('issez-s155:15672','test','test','iAuthor','iAuthor',{
//     "count": 5,"encoding": "auto","ackmode":"ack_requeue_true"
// }).then(x=>{
//     console.log(x);
// }).catch(e=>{
//     console.log(e);
// })

module.exports.getMessageFromQueue = getMessageFromQueue;