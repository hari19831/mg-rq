
var axios = require('axios');
var btoa = require('btoa');


//---Imports---||
var featureSchemaImport = require('./brokerFeatureSchema');
var featureSchema = featureSchemaImport.brokerFeatureSchema;

function createUser(domain, u_name, u_password, user, options) { //---API Endpoint - /api/users/${user}	 [PUT]
    let start = new Date().getTime(); let end; let time;
    return new Promise(async (resolve, reject) => {
        try {
                if (domain == undefined || u_name == undefined || u_password == undefined || user == undefined) {
                    end = new Date().getTime();
                    time = end - start;
                    resolve(new featureSchema(false, false, true, [], 'createUser', time, 'Null', `one or more parameters required is missing`)); //----->> LimitationEndPoint
                }
            
                let encodedPass = btoa(`${u_name}:${u_password}`) || '';
                let config = {
                    method: 'put',
                    url: `http://${domain}/api/users/${user}`,
                    headers: {
                        'Authorization': `Basic ${encodedPass}`,
                        'Content-Type': 'application/json'
                    },
                    data: options
                };
                axios(config).then(function (response) {
                    end = new Date().getTime();
                    time = end - start;
                    resolve(new featureSchema(true, false, false, response.data, 'createUser', time, 'Null', 'Null')); //----->> SuccessEndPoint
                }).catch(function (err2) {
                    let resp = (err2.response == undefined) ? [] : err2.response.data;
                    end = new Date().getTime();
                    time = end - start;
                    resolve(new featureSchema(false, true, false, resp, 'createUser', time, err2.toString(), 'Null')); //----->> ErrorEndPoint
                });
        } catch (err1) {
            end = new Date().getTime();
            time = end - start;
            console.log(err1);
            resolve(new featureSchema(false, true, false, [], 'createUser', time, err1.toString(), 'Null')); //----->> ErrorEndPoint
        }
    });
}
// createUser('issez-s155:15672','test','test','test1',{"password":"secret","tags":"administrator"}).then(x=>{
//     console.log(x);
// }).catch(e=>{
//     console.log(e);
// })

module.exports.createUser = createUser;