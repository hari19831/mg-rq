
var axios = require('axios');
var btoa = require('btoa');

//---Imports---||
var featureSchemaImport = require('./brokerFeatureSchema');
var featureSchema = featureSchemaImport.brokerFeatureSchema;

function deleteUservHostPermission(domain, u_name, u_password, vhost, user) { //---API Endpoint - /api/permissions/${vhost}/${user}	 [DELETE]
    let start = new Date().getTime(); let end; let time;
    return new Promise(async (resolve, reject) => {
        try {
				// let userList = await listUsers(domain, u_name, u_password);
				// if (userList.isError == true) { throw userList }
				// let users = [];
				// userList.result.forEach(element => {
				//     users.push(element.name)
				// });
				// if (!users.includes(user)) {
				//     end = new Date().getTime();
				//     time = end - start;
				//     resolve(new featureSchema(false, false, [], 'deleteUservHostPermission', time, null, true, "No such user available.")); //----->> ErrorEndPoint
				//     return;
				// }
				// let vhostList = await listVhosts(domain, u_name, u_password);
				// if (vhostList.isError == true) { throw vhostList }
				// let vhosts = [];
				// vhostList.result.forEach(element => {
				//     vhosts.push(element.name)
				// });
				// if (!vhosts.includes(vhost)) {
				//     end = new Date().getTime();
				//     time = end - start;
				//     resolve(new featureSchema(false, false, [], 'deleteUservHostPermission', time, null, true, "No such vhost available.")); //----->> ErrorEndPoint
				//     return;
				// }
				if (domain == undefined || u_name == undefined || u_password == undefined || vhost == undefined || user == undefined) {
					end = new Date().getTime();
					time = end - start;
					resolve(new featureSchema(false, false, true, [], 'deleteUservHostPermission', time, 'Null', `one or more parameters required is missing`)); //----->> LimitationEndPoint
				}
				
				let encodedPass = btoa(`${u_name}:${u_password}`) || '';
				let config = {
					method: 'delete',
					url: `http://${domain}/api/permissions/${vhost}/${user}`,
					headers: {
						'Authorization': `Basic ${encodedPass}`,
						'Content-Type': 'application/json'
					}
				};
				axios(config).then(function (response) {
					end = new Date().getTime();
					time = end - start;
					resolve(new featureSchema(true, false, false, response.data, 'deleteUservHostPermission', time, 'Null', 'Null')); //----->> SuccessEndPoint
				}).catch(function (err2) {
					let resp = (err2.response == undefined) ? [] : err2.response.data;
					end = new Date().getTime();
					time = end - start;
					resolve(new featureSchema(false, true, false, resp, 'deleteUservHostPermission', time, err2.toString(), 'Null')); //----->> ErrorEndPoint
				});
        } catch (err1) {
            end = new Date().getTime();
            time = end - start;
            console.log(err1);
            resolve(new featureSchema(false, true, false, [], 'deleteUservHostPermission', time, err1.toString(), 'Null')); //----->> ErrorEndPoint
        }
    });
}
// deleteUservHostPermission('issez-s155:15672','test','test','iAuthor','test1').then(x=>{
//     console.log(x);
// }).catch(e=>{
//     console.log(e);
// })


module.exports.deleteUservHostPermission = deleteUservHostPermission;