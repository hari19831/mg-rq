var axios = require('axios');
var btoa = require('btoa');


//---Imports---||
var featureSchemaImport = require('./brokerFeatureSchema');
var featureSchema = featureSchemaImport.brokerFeatureSchema;


function updateActions(domain, u_name, u_password, vhost, queue, options) { //---API Endpoint - /api/queues/${vhost}/${queue}/actions [POST]
    let start = new Date().getTime(); let end; let time;
    return new Promise(async (resolve, reject) => {
        try {
            if (domain == undefined || u_name == undefined || u_password == undefined || vhost == undefined || queue == undefined || options == undefined) {
                end = new Date().getTime();
                time = end - start;
                resolve(new featureSchema(false, false, true, [], 'updateActions', time, 'Null', `one or more parameters required is missing`)); //----->> LimitationEndPoint
            }
			
			let encodedPass = btoa(`${u_name}:${u_password}`) || '';
            let config = {
                method: 'post',
                url: `http://${domain}/api/queues/${vhost}/${queue}/actions`,
                headers: {
                    'Authorization': `Basic ${encodedPass}`,
                    'Content-Type': 'application/json'
                },
                data: options
            };
            axios(config).then(function (response) {
                end = new Date().getTime();
                time = end - start;
                resolve(new featureSchema(true, false, false, response.data, 'updateActions', time, 'Null', 'Null')); //----->> SuccessEndPoint
            }).catch(function (err2) {
                let resp = (err2.response == undefined) ? [] : err2.response.data;
                end = new Date().getTime();
                time = end - start;
                resolve(new featureSchema(false, true, false, resp, 'updateActions', time, err2.toString(), 'Null')); //----->> ErrorEndPoint
            });
        } catch (err1) {
            end = new Date().getTime();
            time = end - start;
            console.log(err1);
            resolve(new featureSchema(false, true, false, [], 'updateActions', time, err1.toString(), 'Null')); //----->> ErrorEndPoint
        }
    });
}
// updateActions('issez-s155:15672','test','test','iAuthor','iAuthor',{
//     "action": "sync"
// }).then(x=>{
//     console.log(x);
// }).catch(e=>{
//     console.log(e);
// })

module.exports.updateActions = updateActions;