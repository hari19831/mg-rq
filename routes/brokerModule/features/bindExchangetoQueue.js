var axios = require('axios');
var btoa = require('btoa');


//---Imports---||
var featureSchemaImport = require('./brokerFeatureSchema');
var featureSchema = featureSchemaImport.brokerFeatureSchema;


function bindExchangetoQueue(domain, u_name, u_password, vhost, exchange, queue, options) { //---API Endpoint - /api/bindings/${vhost}/e/${exchange}/q/${queue} [POST]
    let start = new Date().getTime(); let end; let time;
    return new Promise(async (resolve, reject) => {
        try {
                if (domain == undefined || u_name == undefined || u_password == undefined || vhost == undefined || exchange == undefined || queue == undefined) {
                    end = new Date().getTime();
                    time = end - start;
                    resolve(new featureSchema(false, false, true, [], 'bindExchangetoQueue', time, 'Null', `one or more parameters required is missing`)); //----->> LimitationEndPoint
                }
            
                let encodedPass = btoa(`${u_name}:${u_password}`) || '';
                let config = {
                    method: 'post',
                    url: `http://${domain}/api/bindings/${vhost}/e/${exchange}/q/${queue}`,
                    headers: {
                        'Authorization': `Basic ${encodedPass}`,
                        'Content-Type': 'application/json'
                    },
                    data: options
                };
                axios(config).then(function (response) {
                    end = new Date().getTime();
                    time = end - start;
                    resolve(new featureSchema(true, false, false, response.data, 'BindExchangetoQueue', time, 'Null', 'Null')); //----->> SuccessEndPoint
                }).catch(function (err2) {
                    let resp = (err2.response == undefined) ? [] : err2.response.data;
                    end = new Date().getTime();
                    time = end - start;
                    resolve(new featureSchema(false, true, false, resp, 'BindExchangetoQueue', time, err2.toString(), 'Null')); //----->> ErrorEndPoint
                });
        } catch (err1) {
            end = new Date().getTime();
            time = end - start;
            console.log(err1);
            resolve(new featureSchema(false, true, false, [], 'BindExchangetoQueue', time, err1.toString(), 'Null')); //----->> ErrorEndPoint
        }
    });
}
// bindExchangetoQueue('issez-s155:15672','test','test','iAuthor','iAuthor','iAuthor',{"routing_key":"my_routing_key","arguments":{}}).then(x=>{
//     console.log(x);
// }).catch(e=>{
//     console.log(e);
// })

module.exports.bindExchangetoQueue = bindExchangetoQueue;