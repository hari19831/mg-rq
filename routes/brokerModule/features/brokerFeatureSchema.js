'use strict';

function featureSchema(success, error, limitation, result, type, time, errMsg, limitationMsg) {
    this.isSuccess = (success == undefined) ? false : success;
    this.isError = (error == undefined) ? false : error;
    this.isLimitation = (limitation == undefined) ? true : limitation;
    this.result = result || [];
    this.featureType = type || 'basic';
    this.timeTaken_ms = time || 0;
    this.errMsg = errMsg || 'Null';
    this.limitationMsg = limitationMsg || 'Null';
}

module.exports.brokerFeatureSchema = featureSchema;