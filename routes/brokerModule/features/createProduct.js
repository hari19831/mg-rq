var defaultConfig = require("../config/brokerConfig.json");
var db = require("../config/db").db;
var cryptr = require("../config/cryptr").cryptr;
var featureSchemaImport = require('./brokerFeatureSchema');
var generatePassword = require("password-generator");
var processActions_brokerFeature = require('../../brokerFeatureRouter').processActions_brokerFeature;
var featureSchema = featureSchemaImport.brokerFeatureSchema;

function createProduct(input) { //---API Endpoint - /api/exchanges/${vhost}/${exchange} [PUT]
    let start = new Date().getTime(); let end; let time;
    return new Promise(async (resolve, reject) => {
        try {
            let productCode = input.productCode;
            if (productCode == undefined) {
                end = new Date().getTime();
                time = end - start;
                resolve(new featureSchema(false, false, true, [], 'createProduct', time, 'Null', 'productCode inputs are required to process')); //----->> LimitaionEndPoint
            }
            else {
                if (db.exists(`/db/${productCode}`)) {
                    let productCodeDetails = db.getData(`/db/${productCode}`);
                    end = new Date().getTime();
                    time = end - start;
                    resolve(new featureSchema(true, false, false, productCodeDetails, 'createProduct', time, 'Null', 'Null'));
                } else {
                    let createVhost = {
                        method: "createVhost",
                        productCode: input.productCode,
                        username: defaultConfig.admin.user,
                        password: defaultConfig.admin.password
                    }
                    let vhost = await processActions_brokerFeature(createVhost);
                    if (vhost.isSuccess == false) {
                        resolve(vhost);
                    } else {
                        let createUser = {
                            user: productCode + "_user",
                            username: productCode + "_user",
                            password: generatePassword(12, false),
                            tags: "administrator",
                            method: "createuser",
                            productCode: productCode
                        }
                        let User = await processActions_brokerFeature(createUser);
                        if (User.isSuccess == false) {
                            resolve(user);
                        } else {
                            let usermap = {
                                configure: ".*",
                                read: ".*",
                                write: ".*",
                                productCode: productCode,
                                username: createUser.user,
                                password: createUser.password,
                                method: "updateUservhostpermission"
                            };
                            let map = await processActions_brokerFeature(usermap);
                            if (map.isSuccess == false) {
                                resolve(user);
                            } else {
                                let productCodeDetails = {
                                    productCode: productCode,
                                    token: cryptr.encrypt(JSON.stringify({
                                        username: createUser.user,
                                        password: createUser.password,
                                        vhost: productCode
                                    }))
                                }
                                await db.push(`/db/${productCode}`, productCodeDetails, true);
                                var eventRegistry = require('../../wrapperEventRegistry');
                                eventRegistry.ProductEvent(productCode);
                                end = new Date().getTime();
                                time = end - start;
                                resolve(new featureSchema(true, false, false, productCodeDetails, 'createProduct', time, 'Null', 'Null'));
                            }
                        }
                    }
                }
            }
        } catch (err1) {
            end = new Date().getTime();
            time = end - start;
            console.log(err1);
            resolve(new featureSchema(false, true, false, [], 'createProduct', time, err1.toString(), 'Null')); //----->> ErrorEndPoint
        }
    });
}

module.exports.createProduct = createProduct;