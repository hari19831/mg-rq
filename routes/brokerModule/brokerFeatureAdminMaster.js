'use strict'

var brokerFeatureAdminMaster = {};

    brokerFeatureAdminMaster.admin = {};
    brokerFeatureAdminMaster.admin.create = {};
    brokerFeatureAdminMaster.admin.read   = {};
    brokerFeatureAdminMaster.admin.update = {};
    brokerFeatureAdminMaster.admin.delete = {};

    brokerFeatureAdminMaster.user  = {};
    brokerFeatureAdminMaster.user.create = {};
    brokerFeatureAdminMaster.user.read   = {};
    brokerFeatureAdminMaster.user.update = {};
    brokerFeatureAdminMaster.user.delete = {};


//---Imports---||
var brokerFeatureCRUDmaster  = require('./brokerFeatureCRUDmaster').brokerFeatureCRUDmaster;


//------------------------------------Admin Actions-------------------------------------------------------||

//--CREATE--||
    brokerFeatureAdminMaster.admin.create.createUser  = brokerFeatureCRUDmaster.create.createUser;
    brokerFeatureAdminMaster.admin.create.createVhost = brokerFeatureCRUDmaster.create.createVhost;
//--READ--||
    brokerFeatureAdminMaster.admin.read.aboutBroker = brokerFeatureCRUDmaster.read.aboutBroker;
    brokerFeatureAdminMaster.admin.read.aboutUser   = brokerFeatureCRUDmaster.read.aboutUser;
    brokerFeatureAdminMaster.admin.read.listUsers   = brokerFeatureCRUDmaster.read.listUsers; 
    brokerFeatureAdminMaster.admin.read.listVhosts  = brokerFeatureCRUDmaster.read.listVhosts;
    brokerFeatureAdminMaster.admin.read.isVhostLive = brokerFeatureCRUDmaster.read.isVhostLive;
//--UPDATE--||
    brokerFeatureAdminMaster.admin.update.updateUserVhostPermission = brokerFeatureCRUDmaster.update.updateUserVhostPermission;
//--DELETE--||
    brokerFeatureAdminMaster.admin.delete.deleteUser                = brokerFeatureCRUDmaster.delete.deleteUser;
    brokerFeatureAdminMaster.admin.delete.deleteVhost               = brokerFeatureCRUDmaster.delete.deleteVhost;
    brokerFeatureAdminMaster.admin.delete.deleteUserVhostPermission = brokerFeatureCRUDmaster.delete.deleteUserVhostPermission;


//---------------------------------------------------------------------------------------------------------||


//------------------------------------User Actions---------------------------------------------------------||

//--CREATE--||
    brokerFeatureAdminMaster.user.create.createExchange  = brokerFeatureCRUDmaster.create.createExchange;
    brokerFeatureAdminMaster.user.create.createQueue     = brokerFeatureCRUDmaster.create.createQueue;
//--READ--||
    brokerFeatureAdminMaster.user.read.listQueue                       = brokerFeatureCRUDmaster.read.listQueue;
    brokerFeatureAdminMaster.user.read.listConsumers                   = brokerFeatureCRUDmaster.read.listConsumers;
    brokerFeatureAdminMaster.user.read.listExchanges                   = brokerFeatureCRUDmaster.read.listExchanges;
    brokerFeatureAdminMaster.user.read.listQueueBindings               = brokerFeatureCRUDmaster.read.listQueueBindings;
    brokerFeatureAdminMaster.user.read.listExchangeBindingsSource      = brokerFeatureCRUDmaster.read.listExchangeBindingSource;
    brokerFeatureAdminMaster.user.read.listExchangeBindingsDestination = brokerFeatureCRUDmaster.read.listExchangeBindingDestination;
    brokerFeatureAdminMaster.user.read.consumeMessage                  = brokerFeatureCRUDmaster.read.consumeMessage;

//--UPDATE--||
    brokerFeatureAdminMaster.user.update.bindExchangeToQueue = brokerFeatureCRUDmaster.update.bindExchangeToQueue;
    brokerFeatureAdminMaster.user.update.publishMessage      = brokerFeatureCRUDmaster.update.publishMessage;
    brokerFeatureAdminMaster.user.update.updateQueueAction   = brokerFeatureCRUDmaster.update.updateQueueAction;
//--DELETE--||
    brokerFeatureAdminMaster.user.delete.deleteExchange     = brokerFeatureCRUDmaster.delete.deleteExchange;
    brokerFeatureAdminMaster.user.delete.deleteQueue        = brokerFeatureCRUDmaster.delete.deleteQueue;
    brokerFeatureAdminMaster.user.delete.deleteQueueMessage = brokerFeatureCRUDmaster.delete.deleteQueueMessage;


//---------------------------------------------------------------------------------------------------------||

// console.log(`Admin Actions
//                             CREATE
//                                     COUNT - ${Object.keys(brokerFeatureAdminMaster.admin.create).length}  ${Object.keys(brokerFeatureAdminMaster.admin.create)}
//                             READ
//                                     COUNT - ${Object.keys(brokerFeatureAdminMaster.admin.read).length}  ${Object.keys(brokerFeatureAdminMaster.admin.read)}
//                             UPDATE
//                                     COUNT - ${Object.keys(brokerFeatureAdminMaster.admin.update).length}  ${Object.keys(brokerFeatureAdminMaster.admin.update)}
//                             DELETE
//                                     COUNT - ${Object.keys(brokerFeatureAdminMaster.admin.delete).length}  ${Object.keys(brokerFeatureAdminMaster.admin.delete)}`);

// console.log(`User Actions
//                             CREATE
//                                     COUNT - ${Object.keys(brokerFeatureAdminMaster.user.create).length}  ${Object.keys(brokerFeatureAdminMaster.user.create)}
//                             READ
//                                     COUNT - ${Object.keys(brokerFeatureAdminMaster.user.read).length}  ${Object.keys(brokerFeatureAdminMaster.user.read)}
//                             UPDATE
//                                     COUNT - ${Object.keys(brokerFeatureAdminMaster.user.update).length}  ${Object.keys(brokerFeatureAdminMaster.user.update)}
//                             DELETE
//                                     COUNT - ${Object.keys(brokerFeatureAdminMaster.user.delete).length}  ${Object.keys(brokerFeatureAdminMaster.user.delete)}`);

module.exports.brokerFeatureAdminMaster = brokerFeatureAdminMaster;