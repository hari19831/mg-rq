var wrapperrFeatureMaster = {};

var createProduct = require("./features/createProduct").createProduct;

try{
    wrapperrFeatureMaster.createProduct               = createProduct;
}catch(e){
    console.log(e);
}

module.exports.wrapperrFeatureMaster = wrapperrFeatureMaster;