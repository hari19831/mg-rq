'use strict';

var fs = require('fs');

//----FileImports----||
    var defaultConfig = JSON.parse(fs.readFileSync(`./routes/brokerModule/config/brokerConfig.json`,`utf-8`)); //Fromserver
    // var defaultConfig = JSON.parse(fs.readFileSync(`./../config/brokerConfig.json`,`utf-8`)); //Standalone
    //console.log(defaultConfig);


//----ModuleImports----||
    var listQueue     = require('./../brokerFeatureAdminMaster').brokerFeatureAdminMaster.user.read.listQueue;
    var validateVhostImport = require('./validateVhost').validateVhost;
   

// console.log(listUsers); 
    
function featureSchema(success, error,result, type, time, errMsg) {
    this.isSuccess = (success == undefined) ? false : success;
    this.isError = (error == undefined) ? false : error;
    this.result = result || [];
    this.featureType = type || 'validation';
    this.timeTaken_ms = time || 0;
    this.errMsg = errMsg || 'Null';

}



function validate_queue(vhost, queue){
    let start = new Date().getTime(); let end; let time;
    return new Promise(async (resolve, reject)=>{
        try{
            if(vhost == undefined || queue == undefined){
                resolve(new featureSchema(false, true, {isExists: false}, 'validate_queue', time, 'All inputs are required to process')); //----->> FailureEndPoint
                return;
            }
            let vhostName = vhost;
            let queueName = queue;
            let vhostKey  = (vhost=='') ? '/' : vhost;

            let validateVhost = await validateVhostImport(vhostKey);

            if( ! validateVhost.isSuccess || ! validateVhost.result.isExists){
                resolve(new featureSchema(false, true, {isExists: false}, 'validate_queue', time, 'Invalid vhost input received')); //----->> FailureEndPoint
                return;
            }

            let queueList = await listQueue(defaultConfig.admin.domain, vhostName, defaultConfig.admin.user, defaultConfig.admin.password); //FetchQueues
            //console.log(queueList);
            if(queueList.isSuccess){
                let finalList = []; 
                let result = {}; 
                    result.isExists = false;
                    queueList.result.forEach((v,i,a)=>{
                         if(v.vhost == vhostKey){
                            finalList.push(v.name);
                         }
                        });
                //console.log(finalList); 
                if(finalList.indexOf(queueName) != -1){
                    result.isExists = true;
                }
                end = new Date().getTime();
                time = end - start;
                resolve(new featureSchema(true, false, result, 'validate_queue', time, 'Null')); //----->> SuccessEndPoint
                return;
            }else{
                end = new Date().getTime();
                time = end - start;
                resolve(new featureSchema(false, true, [], 'validate_queue', time, queueList.errMsg)); //----->> FailureEndPoint
                return; 
            }
        }catch(err1){
            end = new Date().getTime();
            time = end - start;
            resolve(new featureSchema(false, true, [], 'validate_queue', time, err1.toString())); //----->> ErrorEndPoint
            return;
        }

    });
    
}

// validate_queue('harry_host','engine_2_publish').then((msg)=>{ console.log(msg); })

module.exports.validatQueue = validate_queue;

