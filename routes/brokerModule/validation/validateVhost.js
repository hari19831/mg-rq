'use strict';

var fs = require('fs');

//----FileImports----||
    var defaultConfig = JSON.parse(fs.readFileSync(`./routes/brokerModule/config/brokerConfig.json`,`utf-8`)); //Fromserver
    // var defaultConfig = JSON.parse(fs.readFileSync(`./../config/brokerConfig.json`,`utf-8`)); //Standalone
    //console.log(defaultConfig);


//----ModuleImports----||
    var listVhosts    = require('./../brokerFeatureAdminMaster').brokerFeatureAdminMaster.admin.read.listVhosts;

// console.log(listUsers); 
    
function featureSchema(success, error,result, type, time, errMsg) {
    this.isSuccess = (success == undefined) ? false : success;
    this.isError = (error == undefined) ? false : error;
    this.result = result || [];
    this.featureType = type || 'validation';
    this.timeTaken_ms = time || 0;
    this.errMsg = errMsg || 'Null';

}

// validate_user('guest').then((msg)=>{console.log(msg);})

function validate_vhost(vhost){
    let start = new Date().getTime(); let end; let time;
    return new Promise(async (resolve, reject)=>{
        try{
            if(vhost == undefined){
                resolve(new featureSchema(false, true, {isExists: false}, 'validate_vhost', time, 'Input is required to process')); //----->> FailureEndPoint
                return;
            }
            let vhostName = vhost;
            let vhostKey  = (vhost=='') ? '/' : vhost;
            let vhostList = await listVhosts(defaultConfig.admin.domain, defaultConfig.admin.user, defaultConfig.admin.password); //---->> FetchVhosts
            //console.log(userList);
            if(vhostList.isSuccess){
                let finalList = []; 
                let result = {}; 
                    result.isExists = false;
                vhostList.result.forEach((v,i,a)=>{ finalList.push(v.name); });
                //console.log(finalList);
                if(finalList.indexOf(vhostKey) != -1){
                    result.isExists = true;
                }
                end = new Date().getTime();
                time = end - start;
                resolve(new featureSchema(true, false, result, 'validate_vhost', time, 'Null')); //----->> SuccessEndPoint
                return;
            }else{
                end = new Date().getTime();
                time = end - start;
                resolve(new featureSchema(false, true, [], 'validate_vhost', time, vhostList.errMsg)); //----->> FailureEndPoint
                return; 
            }
        }catch(err1){
            end = new Date().getTime();
            time = end - start;
            resolve(new featureSchema(false, true, [], 'validate_vhost', time, err1.toString())); //----->> ErrorEndPoint
            return;
        }

    });
    
}

//validate_vhost('harry_host').then((msg)=>{ console.log(msg); })

module.exports.validateVhost = validate_vhost;

