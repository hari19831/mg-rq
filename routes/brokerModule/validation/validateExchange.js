'use strict';

var fs = require('fs');

//----FileImports----||
    var defaultConfig = JSON.parse(fs.readFileSync(`./routes/brokerModule/config/brokerConfig.json`,`utf-8`)); //Fromserver
    // var defaultConfig = JSON.parse(fs.readFileSync(`./../config/brokerConfig.json`,`utf-8`)); //Standalone
    //console.log(defaultConfig);


//----ModuleImports----||
    var listExchanges = require('./../brokerFeatureAdminMaster').brokerFeatureAdminMaster.user.read.listExchanges;
    var validateVhostImport = require('./validateVhost').validateVhost;

// console.log(listUsers); 
    
function featureSchema(success, error,result, type, time, errMsg) {
    this.isSuccess = (success == undefined) ? false : success;
    this.isError = (error == undefined) ? false : error;
    this.result = result || [];
    this.featureType = type || 'validation';
    this.timeTaken_ms = time || 0;
    this.errMsg = errMsg || 'Null';
}



function validate_exchange(vhost, exchange){
    let start = new Date().getTime(); let end; let time;
    return new Promise(async (resolve, reject)=>{
        try{
            if(vhost == undefined || exchange == undefined){
                resolve(new featureSchema(false, true, {isExists: false}, 'validate_exchange', time, 'All inputs are required to process')); //----->> FailureEndPoint
                return;
            }
            let vhostName = vhost;
            let exchangeName = exchange;
            let vhostKey  = (vhost=='') ? '/' : vhost;

            let validateVhost = await validateVhostImport(vhostKey);

            if( ! validateVhost.isSuccess || ! validateVhost.result.isExists){
                resolve(new featureSchema(false, true, {isExists: false}, 'validate_exchange', time, 'Invalid vhost input received')); //----->> FailureEndPoint
                return;
            }

            let exchangeList = await listExchanges(defaultConfig.admin.domain, vhostName, defaultConfig.admin.user, defaultConfig.admin.password); //FetchQueues
            //console.log(exchangeList);
            if(exchangeList.isSuccess){
                let finalList = []; 
                let result = {}; 
                    result.isExists = false;
                    exchangeList.result.forEach((v,i,a)=>{
                         if(v.vhost == vhostKey){
                            finalList.push(v.name);
                         }
                    });
                // console.log(finalList); 
                if(finalList.indexOf(exchangeName) != -1){
                    result.isExists = true;
                }
                end = new Date().getTime();
                time = end - start;
                resolve(new featureSchema(true, false, result, 'validate_exchange', time, 'Null')); //----->> SuccessEndPoint
                return;
            }else{
                end = new Date().getTime();
                time = end - start;
                resolve(new featureSchema(false, true, [], 'validate_exchange', time, exchangeList.errMsg)); //----->> FailureEndPoint
                return; 
            }
        }catch(err1){
            end = new Date().getTime();
            time = end - start;
            resolve(new featureSchema(false, true, [], 'validate_exchange', time, err1.toString())); //----->> ErrorEndPoint
            return;
        }

    });
    
}

// validate_exchange('harry_host','harry_headers').then((msg)=>{ console.log(msg); })

module.exports.validateExchange = validate_exchange;
