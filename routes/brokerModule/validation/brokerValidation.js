'use strict';

var fs = require('fs');

//----FileImports----||
    var defaultConfig = JSON.parse(fs.readFileSync(`./routes/brokerModule/config/brokerConfig.json`,`utf-8`)); //Fromserver
    // var defaultConfig = JSON.parse(fs.readFileSync(`./../config/brokerConfig.json`,`utf-8`)); //Standalone
    //console.log(defaultConfig);


//----ModuleImports----||
    var listUsers     = require('./../brokerFeatureAdminMaster').brokerFeatureAdminMaster.admin.read.listUsers;
    var listVhosts    = require('./../brokerFeatureAdminMaster').brokerFeatureAdminMaster.admin.read.listVhosts;
    var listQueue     = require('./../brokerFeatureAdminMaster').brokerFeatureAdminMaster.user.read.listQueue;
    var listExchanges = require('./../brokerFeatureAdminMaster').brokerFeatureAdminMaster.user.read.listExchanges;

// console.log(listUsers); 
    
function featureSchema(success, error,result, type, time, errMsg) {
    this.isSuccess = (success == undefined) ? false : success;
    this.isError = (error == undefined) ? false : error;
    this.result = result || [];
    this.featureType = type || 'validation';
    this.timeTaken_ms = time || 0;
    this.errMsg = errMsg || 'Null';

}


var validateBroker = {};
    validateBroker.validateUser     = validate_user;
    validateBroker.validateVhost    = validate_vhost;
    validateBroker.validateQueue    = validate_queue;
    validateBroker.validateExchange = validate_exchange;

function validate_user(user){
    let start = new Date().getTime(); let end; let time;
    return new Promise(async (resolve, reject)=>{
        try{
            if(user == undefined){
                resolve(new featureSchema(false, true, {isExists: false}, 'validate_user', time, 'Input is required to process')); //----->> FailureEndPoint
                return;
            }
            let userName = user;
            let userList = await listUsers(defaultConfig.admin.domain, defaultConfig.admin.user, defaultConfig.admin.password); //---->> FetchUsers
            //console.log(userList);
            if(userList.isSuccess){
                let finalList = []; 
                let result = {}; 
                    result.isExists = false;
                userList.result.forEach((v,i,a)=>{ finalList.push(v.name); });
                //console.log(finalList);
                if(finalList.indexOf(userName) != -1){
                    result.isExists = true;
                }
                end = new Date().getTime();
                time = end - start;
                resolve(new featureSchema(true, false, result, 'validate_user', time, 'Null')); //----->> SuccessEndPoint
                return;
            }else{
                end = new Date().getTime();
                time = end - start;
                resolve(new featureSchema(false, true, [], 'validate_user', time, userList.errMsg)); //----->> FailureEndPoint
                return; 
            }
        }catch(err1){
            end = new Date().getTime();
            time = end - start;
            resolve(new featureSchema(false, true, [], 'validate_user', time, err1.toString())); //----->> ErrorEndPoint
            return;
        }

    });
    
}

// validate_user('guest').then((msg)=>{console.log(msg);})

function validate_vhost(vhost){
    let start = new Date().getTime(); let end; let time;
    return new Promise(async (resolve, reject)=>{
        try{
            if(vhost == undefined){
                resolve(new featureSchema(false, true, {isExists: false}, 'validate_vhost', time, 'Input is required to process')); //----->> FailureEndPoint
                return;
            }
            let vhostName = vhost;
            let vhostList = await listVhosts(defaultConfig.admin.domain, defaultConfig.admin.user, defaultConfig.admin.password); //---->> FetchVhosts
            //console.log(userList);
            if(vhostList.isSuccess){
                let finalList = []; 
                let result = {}; 
                    result.isExists = false;
                vhostList.result.forEach((v,i,a)=>{ finalList.push(v.name); });
                //console.log(finalList);
                if(finalList.indexOf(vhostName) != -1){
                    result.isExists = true;
                }
                end = new Date().getTime();
                time = end - start;
                resolve(new featureSchema(true, false, result, 'validate_vhost', time, 'Null')); //----->> SuccessEndPoint
                return;
            }else{
                end = new Date().getTime();
                time = end - start;
                resolve(new featureSchema(false, true, [], 'validate_vhost', time, vhostList.errMsg)); //----->> FailureEndPoint
                return; 
            }
        }catch(err1){
            end = new Date().getTime();
            time = end - start;
            resolve(new featureSchema(false, true, [], 'validate_vhost', time, err1.toString())); //----->> ErrorEndPoint
            return;
        }

    });
    
}

// validate_vhost('harry_host').then((msg)=>{ console.log(msg); })

function validate_queue(vhost, queue){
    let start = new Date().getTime(); let end; let time;
    return new Promise(async (resolve, reject)=>{
        try{
            if(vhost == undefined || queue == undefined){
                resolve(new featureSchema(false, true, {isExists: false}, 'validate_queue', time, 'All inputs are required to process')); //----->> FailureEndPoint
                return;
            }
            let vhostName = vhost;
            let queueName = queue;
            let vhostKey  = (vhost=='') ? '/' : vhost;

            let validateVhost = await validate_vhost(vhostKey);

            if( ! validateVhost.isSuccess || ! validateVhost.result.isExists){
                resolve(new featureSchema(false, true, {isExists: false}, 'validate_queue', time, 'Invalid vhost input received')); //----->> FailureEndPoint
                return;
            }

            let queueList = await listQueue(defaultConfig.admin.domain, vhostName, defaultConfig.admin.user, defaultConfig.admin.password); //FetchQueues
            //console.log(queueList);
            if(queueList.isSuccess){
                let finalList = []; 
                let result = {}; 
                    result.isExists = false;
                    queueList.result.forEach((v,i,a)=>{
                         if(v.vhost == vhostKey){
                            finalList.push(v.name);
                         }
                        });
                //console.log(finalList); 
                if(finalList.indexOf(queueName) != -1){
                    result.isExists = true;
                }
                end = new Date().getTime();
                time = end - start;
                resolve(new featureSchema(true, false, result, 'validate_queue', time, 'Null')); //----->> SuccessEndPoint
                return;
            }else{
                end = new Date().getTime();
                time = end - start;
                resolve(new featureSchema(false, true, [], 'validate_queue', time, queueList.errMsg)); //----->> FailureEndPoint
                return; 
            }
        }catch(err1){
            end = new Date().getTime();
            time = end - start;
            resolve(new featureSchema(false, true, [], 'validate_queue', time, err1.toString())); //----->> ErrorEndPoint
            return;
        }

    });
    
}

// validate_queue('harry_host','engine_2_publish').then((msg)=>{ console.log(msg); })

function validate_exchange(vhost, exchange){
    let start = new Date().getTime(); let end; let time;
    return new Promise(async (resolve, reject)=>{
        try{
            if(vhost == undefined || exchange == undefined){
                resolve(new featureSchema(false, true, {isExists: false}, 'validate_exchange', time, 'All inputs are required to process')); //----->> FailureEndPoint
                return;
            }
            let vhostName = vhost;
            let exchangeName = exchange;
            let vhostKey  = (vhost=='') ? '/' : vhost;

            let validateVhost = await validate_vhost(vhostKey);

            if( ! validateVhost.isSuccess || ! validateVhost.result.isExists){
                resolve(new featureSchema(false, true, {isExists: false}, 'validate_exchange', time, 'Invalid vhost input received')); //----->> FailureEndPoint
                return;
            }

            let exchangeList = await listExchanges(defaultConfig.admin.domain, vhostName, defaultConfig.admin.user, defaultConfig.admin.password); //FetchQueues
            //console.log(exchangeList);
            if(exchangeList.isSuccess){
                let finalList = []; 
                let result = {}; 
                    result.isExists = false;
                    exchangeList.result.forEach((v,i,a)=>{
                         if(v.vhost == vhostKey){
                            finalList.push(v.name);
                         }
                    });
                // console.log(finalList); 
                if(finalList.indexOf(exchangeName) != -1){
                    result.isExists = true;
                }
                end = new Date().getTime();
                time = end - start;
                resolve(new featureSchema(true, false, result, 'validate_exchange', time, 'Null')); //----->> SuccessEndPoint
                return;
            }else{
                end = new Date().getTime();
                time = end - start;
                resolve(new featureSchema(false, true, [], 'validate_exchange', time, exchangeList.errMsg)); //----->> FailureEndPoint
                return; 
            }
        }catch(err1){
            end = new Date().getTime();
            time = end - start;
            resolve(new featureSchema(false, true, [], 'validate_exchange', time, err1.toString())); //----->> ErrorEndPoint
            return;
        }

    });
    
}

// validate_exchange('harry_host','harry_headers').then((msg)=>{ console.log(msg); })

// validate_user()
// validate_vhost()
// validate_queue()
// validate_exchange()