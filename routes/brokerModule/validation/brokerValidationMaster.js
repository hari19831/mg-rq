'use strict';

var fs = require('fs');

//----FileImports----||
    var defaultConfig = JSON.parse(fs.readFileSync(`./routes/brokerModule/config/brokerConfig.json`,`utf-8`)); //Fromserver
    // var defaultConfig = JSON.parse(fs.readFileSync(`./../config/brokerConfig.json`,`utf-8`)); //Standalone
    //console.log(defaultConfig);


//----ModuleImports----||
    var validateUserImport = require('./validateUser').validateUser;
    var validateVhostImport = require('./validateVhost').validateVhost;
    var validateQueueImport = require('./validateQueue').validatQueue;
    var validateExchangeImport = require('./validateExchange').validateExchange;

// console.log(listUsers); 
    
function featureSchema(success, error,result, type, time, errMsg) {
    this.isSuccess = (success == undefined) ? false : success;
    this.isError = (error == undefined) ? false : error;
    this.result = result || [];
    this.featureType = type || 'validation';
    this.timeTaken_ms = time || 0;
    this.errMsg = errMsg || 'Null';
}


var validateBroker = {};
    validateBroker.validateUser     = validateUserImport;
    validateBroker.validateVhost    = validateVhostImport;
    validateBroker.validateQueue    = validateQueueImport;
    validateBroker.validateExchange = validateExchangeImport;

// validateBroker.validateUser('guest').then((msg)=>{ console.log(msg); }) 
// validateBroker.validateVhost('harry_host').then((msg)=>{ console.log(msg); }) 
// validateBroker.validateQueue('harry_host','engine_2_publish').then((msg)=>{ console.log(msg); })    
// validateBroker.validateExchange('harry_host','harry_headers').then((msg)=>{ console.log(msg); })

module.exports.brokerValidationMaster = validateBroker;

