'use strict';

var fs = require('fs');

//----FileImports----||
    var defaultConfig = JSON.parse(fs.readFileSync(`./routes/brokerModule/config/brokerConfig.json`,`utf-8`)); //Fromserver
    // var defaultConfig = JSON.parse(fs.readFileSync(`./../config/brokerConfig.json`,`utf-8`)); //Standalone
    //console.log(defaultConfig);


//----ModuleImports----||
    var listUsers     = require('./../brokerFeatureAdminMaster').brokerFeatureAdminMaster.admin.read.listUsers;

// console.log(listUsers); 
    
function featureSchema(success, error,result, type, time, errMsg) {
    this.isSuccess = (success == undefined) ? false : success;
    this.isError = (error == undefined) ? false : error;
    this.result = result || [];
    this.featureType = type || 'validation';
    this.timeTaken_ms = time || 0;
    this.errMsg = errMsg || 'Null';

}



function validate_user(user){
    let start = new Date().getTime(); let end; let time;
    return new Promise(async (resolve, reject)=>{
        try{
            if(user == undefined){
                resolve(new featureSchema(false, true, {isExists: false}, 'validate_user', time, 'Input is required to process')); //----->> FailureEndPoint
                return;
            }
            let userName = user;
            let userList = await listUsers(defaultConfig.admin.domain, defaultConfig.admin.user, defaultConfig.admin.password); //---->> FetchUsers
            //console.log(userList);
            if(userList.isSuccess){
                let finalList = []; 
                let result = {}; 
                    result.isExists = false;
                userList.result.forEach((v,i,a)=>{ finalList.push(v.name); });
                //console.log(finalList);
                if(finalList.indexOf(userName) != -1){
                    result.isExists = true;
                }
                end = new Date().getTime();
                time = end - start;
                resolve(new featureSchema(true, false, result, 'validate_user', time, 'Null')); //----->> SuccessEndPoint
                return;
            }else{
                end = new Date().getTime();
                time = end - start;
                resolve(new featureSchema(false, true, [], 'validate_user', time, userList.errMsg)); //----->> FailureEndPoint
                return; 
            }
        }catch(err1){
            end = new Date().getTime();
            time = end - start;
            resolve(new featureSchema(false, true, [], 'validate_user', time, err1.toString())); //----->> ErrorEndPoint
            return;
        }

    });
    
}

// validate_user('guest').then((msg)=>{console.log(msg);})

module.exports.validateUser = validate_user;
