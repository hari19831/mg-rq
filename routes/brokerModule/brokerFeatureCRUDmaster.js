'use strict'

var brokerFeatureCRUD = {};
    brokerFeatureCRUD.create = {};
    brokerFeatureCRUD.read   = {};
    brokerFeatureCRUD.update = {};
    brokerFeatureCRUD.delete = {};

//---Imports---||
var brokerFeatureMaster  = require('./brokerFeatureMaster').brokerFeatureMaster;


//---CREATE-ACTION---||
brokerFeatureCRUD.create.createUser     = brokerFeatureMaster.createUser;
brokerFeatureCRUD.create.createVhost    = brokerFeatureMaster.createVhost;
brokerFeatureCRUD.create.createExchange = brokerFeatureMaster.createExchange;
brokerFeatureCRUD.create.createQueue    = brokerFeatureMaster.createQueue;

//---READ-ACTION---||
brokerFeatureCRUD.read.aboutBroker                    = brokerFeatureMaster.aboutBroker;
brokerFeatureCRUD.read.aboutUser                      = brokerFeatureMaster.aboutUser;
brokerFeatureCRUD.read.isVhostLive                    = brokerFeatureMaster.isVhostLive;
brokerFeatureCRUD.read.listVhosts                     = brokerFeatureMaster.listVhosts;
brokerFeatureCRUD.read.listUsers                      = brokerFeatureMaster.listUsers;
brokerFeatureCRUD.read.listExchanges                  = brokerFeatureMaster.listExchanges;
brokerFeatureCRUD.read.listConsumers                  = brokerFeatureMaster.listConsumers;
brokerFeatureCRUD.read.listQueue                      = brokerFeatureMaster.listQueue;
brokerFeatureCRUD.read.listQueueBindings              = brokerFeatureMaster.listQueueBindings;
brokerFeatureCRUD.read.listExchangeBindingSource      = brokerFeatureMaster.listExchangeBindingsSource;
brokerFeatureCRUD.read.listExchangeBindingDestination = brokerFeatureMaster.listExchangeBindingsDestination;
brokerFeatureCRUD.read.consumeMessage                 = brokerFeatureMaster.consumeMessage;

//---UPDATE-ACTION---||
brokerFeatureCRUD.update.bindExchangeToQueue       = brokerFeatureMaster.bindExchangeToQueue;
brokerFeatureCRUD.update.publishMessage            = brokerFeatureMaster.publishMessage;
brokerFeatureCRUD.update.updateQueueAction         = brokerFeatureMaster.updateQueueAction;
brokerFeatureCRUD.update.updateUserVhostPermission = brokerFeatureMaster.updateUserVhostPermission;

//---DELETE-ACTION---||
brokerFeatureCRUD.delete.deleteUser                = brokerFeatureMaster.deleteUser;
brokerFeatureCRUD.delete.deleteVhost               = brokerFeatureMaster.deleteVhost;
brokerFeatureCRUD.delete.deleteUserVhostPermission = brokerFeatureMaster.deleteUserVhostPermission;
brokerFeatureCRUD.delete.deleteExchange            = brokerFeatureMaster.deleteExchange;
brokerFeatureCRUD.delete.deleteQueue               = brokerFeatureMaster.deleteQueue;
brokerFeatureCRUD.delete.deleteQueueMessage        = brokerFeatureMaster.deleteQueueMessage;


// console.log(`CRUD Actions
//                             CREATE
//                                     COUNT - ${Object.keys(brokerFeatureCRUD.create).length}  ${Object.keys(brokerFeatureCRUD.create)}
//                             READ
//                                     COUNT - ${Object.keys(brokerFeatureCRUD.read).length}  ${Object.keys(brokerFeatureCRUD.read)}
//                             UPDATE
//                                     COUNT - ${Object.keys(brokerFeatureCRUD.update).length}  ${Object.keys(brokerFeatureCRUD.update)}
//                             DELETE
//                                     COUNT - ${Object.keys(brokerFeatureCRUD.delete).length}  ${Object.keys(brokerFeatureCRUD.delete)}`);


module.exports.brokerFeatureCRUDmaster = brokerFeatureCRUD;
