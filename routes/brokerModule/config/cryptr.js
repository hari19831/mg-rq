//"cryptr": "^6.0.2"
// const Cryptr = require('cryptr');
// const cryptr = new Cryptr('iMQServiceKey');
const atob = require("atob")
const btoa = require("btoa")

var encodeDecode = {
    "decrypt" : btoa,
    "encrypt" : atob
}

module.exports.cryptr = encodeDecode;