const client = require("../../../ll/vaultActions_method");
var authenticationMask = require('../../../ll/authenticationMask');

var { JsonDB } = require('node-json-db');
var { Config } = require('node-json-db/dist/lib/JsonDBConfig')
var _db = new JsonDB(new Config("products", true, false, '/'));

var db = {
    exists: function (path) {
        return _db.exists(path);
    },
    getData: async function (path) {
        let data =  _db.getData(path);
        data.token = await client.getSecret(data.token);
        return data;
    },
    push: async function name(path, data, override) {
        let token = data.productCode+"Token";
        await client.setSecret(token,data.token)
        data.token = token;
        return _db.push(path, data, override);
    }
}

module.exports.db = db;