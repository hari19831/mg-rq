function optionProcess(body) {
    let options = JSON.parse(JSON.stringify(body));
    delete options.exchange;
    delete options.queue;
    delete options.productCode;
    delete options.method;
    delete options.token;
    for (var key of Object.keys(options)) {
        if(isJson(options[key])){
            options[key] = JSON.parse(options[key]);
        }
    }
    return options;
  }
  
  function isJson(str) {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
  }

  module.exports.isJson = isJson;
  module.exports.optionProcess = optionProcess;