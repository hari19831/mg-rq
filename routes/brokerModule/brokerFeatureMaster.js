'use strict'

var brokerFeatureMaster = {};

//---Imports---||
var aboutBroker                     = require('./features/aboutBroker').aboutBroker;
var aboutUser                       = require('./features/aboutUser').aboutUser;
var listVhosts                      = require('./features/listVhosts').listVhosts;
var listQueue                       = require('./features/listQueue').listQueue;
var isVhostLive                     = require('./features/isVhostLive').isVhostLive;
var listUsers                       = require('./features/listUsers').listUsers;
var listConsumers                   = require('./features/listConsumers').listConsumers;
var listExchanges                   = require('./features/listExchanges').listExchanges;
var listQueueBindings               = require('./features/listQueueBindings').listQueueBindings;
var listExchangeBindingsSource      = require('./features/listExchangeBindingsSource').listExchangeBindingsSource;
var listExchangeBindingsDestination = require('./features/listExchangeBindingsDestination').listExchangeBindingsDestination;

var bindExchangeToQueue       = require('./features/bindExchangetoQueue').bindExchangetoQueue;
var createExchange            = require('./features/createExchange').createExchange;
var createQueue               = require('./features/createQueue').createQueue;
var createUser                = require('./features/createUser').createUser;
var createVhost               = require('./features/createvHost').createvHost;
var publishMessage            = require('./features/publish').publish;
var consumeMessage            = require('./features/getMessageFromQueue').getMessageFromQueue;
var updateQueueAction         = require('./features/updateActions').updateActions;
var updateUserVhostPermission = require('./features/updateUservHostPermission').UpdateUservHostPermission;
var deleteExchange            = require('./features/deleteExchange').deleteExchange;
var deleteQueue               = require('./features/deleteQueue').deleteQueue;
var deleteQueueMessage        = require('./features/deleteQueueData').deleteQueueData;
var deleteUser                = require('./features/deleteUser').deleteUser;
var deleteUserVhostPermission = require('./features/deleteUservHostPermission').deleteUservHostPermission;
var deleteVhost               = require('./features/deletevHost').deletevHost;

try{
    
    brokerFeatureMaster.aboutBroker                     = aboutBroker;
    brokerFeatureMaster.aboutUser                       = aboutUser;
    brokerFeatureMaster.listVhosts                      = listVhosts;
    brokerFeatureMaster.listQueue                       = listQueue;
    brokerFeatureMaster.isVhostLive                     = isVhostLive;
    brokerFeatureMaster.listUsers                       = listUsers;
    brokerFeatureMaster.listConsumers                   = listConsumers;
    brokerFeatureMaster.listExchanges                   = listExchanges;
    brokerFeatureMaster.listQueueBindings               = listQueueBindings;
    brokerFeatureMaster.listExchangeBindingsSource      = listExchangeBindingsSource;
    brokerFeatureMaster.listExchangeBindingsDestination = listExchangeBindingsDestination;

    brokerFeatureMaster.bindExchangeToQueue       = bindExchangeToQueue;
    brokerFeatureMaster.createExchange            = createExchange;
    brokerFeatureMaster.createQueue               = createQueue;
    brokerFeatureMaster.createUser                = createUser;
    brokerFeatureMaster.createVhost               = createVhost;
    brokerFeatureMaster.publishMessage            = publishMessage;
    brokerFeatureMaster.consumeMessage            = consumeMessage;
    brokerFeatureMaster.updateQueueAction         = updateQueueAction;
    brokerFeatureMaster.updateUserVhostPermission = updateUserVhostPermission;
    brokerFeatureMaster.deleteExchange            = deleteExchange;
    brokerFeatureMaster.deleteQueue               = deleteQueue;
    brokerFeatureMaster.deleteQueueMessage        = deleteQueueMessage;
    brokerFeatureMaster.deleteUser                = deleteUser;
    brokerFeatureMaster.deleteUserVhostPermission = deleteUserVhostPermission;
    brokerFeatureMaster.deleteVhost               = deleteVhost;


}catch(e){
    console.log(e);
}

//console.log(Object.keys(brokerFeatureMaster));
module.exports.brokerFeatureMaster = brokerFeatureMaster;