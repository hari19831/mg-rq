'use strict';

var fs = require('fs');
var express = require('express');
var wrapperFeature = express.Router();
var cryptr = require('./brokerModule/config/cryptr').cryptr;
var optionProcess = require('./brokerModule/config/common').optionProcess;
var defaultConfig = JSON.parse(fs.readFileSync(`./routes/brokerModule/config/brokerConfig.json`, `utf-8`)); //Fromserver
var wrapperrFeatureMaster = require('./brokerModule/wrapperFeatureMaster').wrapperrFeatureMaster;
var featureSchemaImport = require('./brokerModule/brokerFeatureSchema');
var brokerFeatureAdminMaster = require('./brokerModule/brokerFeatureAdminMaster').brokerFeatureAdminMaster.user;
var brokerFeatureMasterImport = require('./brokerModule/brokerFeatureMaster').brokerFeatureMaster;
var featureSchema = featureSchemaImport.brokerFeatureSchema;
var keyVault = require("../ll/vaultActions_method");

wrapperFeature.get('/', function (req, res, next) {
  res.send({ 'msg': '...Welcome to wrapper feature router...' });
});

let actionList = {};
for (let key of Object.keys(brokerFeatureAdminMaster)) {
  actionList[key] = Object.keys(brokerFeatureAdminMaster[key])
}

function getActions() {
  let start = new Date().getTime(); let end; let time;
  return new Promise((resolve, reject) => {
    try {
      end = new Date().getTime();
      time = end - start;
      resolve(new featureSchema(true, false, false, actionList, 'getWrapperFeaturesMethods', time, 'Null', 'Null')); //----->> SuccessEndPoint
    } catch (e) {
      end = new Date().getTime();
      time = end - start;
      console.log(e);
      resolve(new featureSchema(false, true, false, [], 'getWrapperFeaturesMethods', time, e.toString(), 'Null')); //----->> ErrorEndPoint
    }
  });
}

wrapperFeature.get('/methods', function (req, res, next) {
  res.setHeader('Content-Type', 'application/json');
  res.setTimeout(20000); //wait atleast for 20 secs

  getActions().then((msg) => {
    res.send(JSON.stringify(msg));
  }).catch((e) => {
    res.send(JSON.stringify(new featureSchema(false, true, false, [], 'getWrapperFeaturesMethods', 0, e.toString(), 'Null')));
  });

});

wrapperFeature.post('/methods', function (req, res, next) {
  res.setHeader('Content-Type', 'application/json');
  res.setTimeout(20000); //wait atleast for 20 secs
  processActions(req.body).then((msg) => {
    res.send(JSON.stringify(msg));
  }).catch((e) => {
    res.send(JSON.stringify(new featureSchema(false, true, false, [], 'validateAction', 0, e.toString(), 'Null')));
  });
});

function processActions(body) {
  let start = new Date().getTime(); let end; let time;
  return new Promise(async (resolve, reject) => {
    try {
      let method = body.method;
      let productCode = body.productCode;
      let queue = body.queue;
      let exchange = body.exchange;
      let activity = body.activity;
      let userDetail = await decryptToken(body);
      let username = userDetail.username;
      let password = userDetail.password;
      let options = optionProcess(body);
      let activityDetails = actionList[activity]
      if (method == undefined || productCode == undefined || activity == undefined) {
        end = new Date().getTime();
        time = end - start;
        resolve(new featureSchema(false, false, true, [], 'WrapperMethods', time, 'Null', 'activity, method, productCode inputs are required to process')); //----->> LimitaionEndPoint
      }
      else if (activityDetails == undefined) {
        end = new Date().getTime();
        time = end - start;
        resolve(new featureSchema(false, false, true, [], 'WrapperMethods', time, 'Null', `Invalid activity performed`)); //----->> LimitaionEndPoint
      }
      else if (activityDetails.filter(x => x.toLowerCase() == activity.toLowerCase()).length > 0) {
        end = new Date().getTime();
        time = end - start;
        resolve(new featureSchema(false, false, true, [], 'WrapperMethods', time, 'Null', `Invalid activity performed, check on activity and method.`)); //----->> LimitaionEndPoint
      } else {
        let output = [];
        let result = [];
        switch (method.toLowerCase()) {
          case 'createproduct':
            result = await wrapperrFeatureMaster.createProduct(body);
            end = new Date().getTime();
            time = end - start;
            if (result.isSuccess) {
              resolve(new featureSchema(true, false, false, result.result, 'WrapperMethods', time, 'Null', 'Null')); //----->> SuccessEndPoint
            } else {
              result.type = 'WrapperMethods';
              result.time = time;
              resolve(result); //----------->>FailureEndpoint
            }
            break;
          case 'listqueue':
            result = await brokerFeatureMasterImport.listQueue(defaultConfig.admin.domain, productCode, username, password);
            result.result.forEach(element => {
              output.push(element.name);
            });
            end = new Date().getTime();
            time = end - start;
            if (result.isSuccess) {
              resolve(new featureSchema(true, false, false, output, 'WrapperMethods', time, 'Null', 'Null')); //----->> SuccessEndPoint
            } else {
              result.type = 'WrapperMethods';
              result.time = time;
              resolve(result); //----------->>FailureEndpoint
            }
            break;
          case 'listexchanges':
            result = await brokerFeatureMasterImport.listExchanges(defaultConfig.admin.domain, productCode, username, password);
            result.result.forEach(element => {
              output.push(element.name);
            });
            end = new Date().getTime();
            time = end - start;
            if (result.isSuccess) {
              resolve(new featureSchema(true, false, false, output, 'WrapperMethods', time, 'Null', 'Null')); //----->> SuccessEndPoint
            } else {
              result.type = 'WrapperMethods';
              result.time = time;
              resolve(result); //----------->>FailureEndpoint
            }
            break;
          case 'listqueuebindings':
            result = await brokerFeatureMasterImport.listQueueBindings(defaultConfig.admin.domain, productCode, queue, username, password);
            result.result.forEach(element => {
              output.push(element.source);
            });
            end = new Date().getTime();
            time = end - start;
            if (result.isSuccess) {
              resolve(new featureSchema(true, false, false, output, 'WrapperMethods', time, 'Null', 'Null')); //----->> SuccessEndPoint
            } else {
              result.type = 'WrapperMethods';
              result.time = time;
              resolve(result); //----------->>FailureEndpoint
            }
            break;
          case 'listexchangebindingssource':
            result = await brokerFeatureMasterImport.listExchangeBindingsSource(defaultConfig.admin.domain, productCode, exchange, username, password);
            output = result.result;
            end = new Date().getTime();
            time = end - start;
            if (result.isSuccess) {
              resolve(new featureSchema(true, false, false, output, 'WrapperMethods', time, 'Null', 'Null')); //----->> SuccessEndPoint
            } else {
              result.type = 'WrapperMethods';
              result.time = time;
              resolve(result); //----------->>FailureEndpoint
            }
            break;
          case 'listexchangebindingsdestination':
            result = await brokerFeatureMasterImport.listExchangeBindingsDestination(defaultConfig.admin.domain, productCode, exchange, username, password);
            output = result.result;
            end = new Date().getTime();
            time = end - start;
            if (result.isSuccess) {
              resolve(new featureSchema(true, false, false, output, 'WrapperMethods', time, 'Null', 'Null')); //----->> SuccessEndPoint
            } else {
              result.type = 'WrapperMethods';
              result.time = time;
              resolve(result); //----------->>FailureEndpoint
            }
            break;
          case 'listconsumers':
            result = await brokerFeatureMasterImport.listConsumers(defaultConfig.admin.domain, productCode, username, password);
            output = [];
            end = new Date().getTime();
            time = end - start;
            for (let element of result.result) {
              if (output.filter(x => x.queue == element.queue.name).length == 0) {
                output.push({
                  "queue": element.queue.name,
                  "productCode": element.queue.vhost,
                  "consumerCount": result.result.filter(x => x.queue.name == element.queue.name).length,
                  "prefetch_count": (result.result.filter(x => x.queue.name == element.queue.name).map(a => { return a.prefetch_count })).reduce((a, b) => a + b, 0)
                })
              }
            }
            if(queue != undefined){
              output = output.filter(x=>x.queue==queue);
            }
            if (result.isSuccess) {
              resolve(new featureSchema(true, false, false, output, 'WrapperMethods', time, 'Null', 'Null')); //----->> SuccessEndPoint
            } else {
              result.type = 'WrapperMethods';
              result.time = time;
              resolve(result); //----------->>FailureEndpoint
            }
            break;
          case 'consumemessage':
            result = await brokerFeatureMasterImport.consumeMessage(defaultConfig.admin.domain, username, password, productCode, queue, options);
            output = result.result;
            end = new Date().getTime();
            time = end - start;
            if (result.isSuccess) {
              resolve(new featureSchema(true, false, false, output, 'WrapperMethods', time, 'Null', 'Null')); //----->> SuccessEndPoint
            } else {
              result.type = 'WrapperMethods';
              result.time = time;
              resolve(result); //----------->>FailureEndpoint
            }
            break;
          case 'createexchange':
            result = await brokerFeatureMasterImport.createExchange(defaultConfig.admin.domain, username, password, productCode, exchange, options);
            output = result.result;
            end = new Date().getTime();
            time = end - start;
            if (result.isSuccess) {
              resolve(new featureSchema(true, false, false, output, 'WrapperMethods', time, 'Null', 'Null')); //----->> SuccessEndPoint
            } else {
              result.type = 'WrapperMethods';
              result.time = time;
              resolve(result); //----------->>FailureEndpoint
            }
            break;
          case 'createqueue':
            result = await brokerFeatureMasterImport.createQueue(defaultConfig.admin.domain, username, password, productCode, queue, options);
            output = result.result;
            end = new Date().getTime();
            time = end - start;
            if (result.isSuccess) {
              resolve(new featureSchema(true, false, false, output, 'WrapperMethods', time, 'Null', 'Null')); //----->> SuccessEndPoint
            } else {
              result.type = 'WrapperMethods';
              result.time = time;
              resolve(result); //----------->>FailureEndpoint
            }
            break;
          case 'deletequeue':
            result = await brokerFeatureMasterImport.deleteQueue(defaultConfig.admin.domain, username, password, productCode, queue, options);
            output = result.result;
            end = new Date().getTime();
            time = end - start;
            if (result.isSuccess) {
              resolve(new featureSchema(true, false, false, output, 'WrapperMethods', time, 'Null', 'Null')); //----->> SuccessEndPoint
            } else {
              result.type = 'WrapperMethods';
              result.time = time;
              resolve(result); //----------->>FailureEndpoint
            }
            break;
          case 'deleteexchange':
            result = await brokerFeatureMasterImport.deleteExchange(defaultConfig.admin.domain, username, password, productCode, exchange);
            output = result.result;
            end = new Date().getTime();
            time = end - start;
            if (result.isSuccess) {
              resolve(new featureSchema(true, false, false, output, 'WrapperMethods', time, 'Null', 'Null')); //----->> SuccessEndPoint
            } else {
              result.type = 'WrapperMethods';
              result.time = time;
              resolve(result); //----------->>FailureEndpoint
            }
            break;
          case 'deletequeuemessage':
            result = await brokerFeatureMasterImport.deleteQueueMessage(defaultConfig.admin.domain, username, password, productCode, queue);
            output = result.result;
            end = new Date().getTime();
            time = end - start;
            if (result.isSuccess) {
              resolve(new featureSchema(true, false, false, output, 'WrapperMethods', time, 'Null', 'Null')); //----->> SuccessEndPoint
            } else {
              result.type = 'WrapperMethods';
              result.time = time;
              resolve(result); //----------->>FailureEndpoint
            }
            break;
          case 'bindexchangetoqueue':
            result = await brokerFeatureMasterImport.bindExchangeToQueue(defaultConfig.admin.domain, username, password, productCode, exchange, queue, options);
            output = result.result;
            end = new Date().getTime();
            time = end - start;
            if (result.isSuccess) {
              resolve(new featureSchema(true, false, false, output, 'WrapperMethods', time, 'Null', 'Null')); //----->> SuccessEndPoint
            } else {
              result.type = 'WrapperMethods';
              result.time = time;
              resolve(result); //----------->>FailureEndpoint
            }
            break;
          case 'updatequeueaction':
            result = await brokerFeatureMasterImport.updateQueueAction(defaultConfig.admin.domain, username, password, productCode, queue, options);
            output = result.result;
            end = new Date().getTime();
            time = end - start;
            if (result.isSuccess) {
              resolve(new featureSchema(true, false, false, output, 'WrapperMethods', time, 'Null', 'Null')); //----->> SuccessEndPoint
            } else {
              result.type = 'WrapperMethods';
              result.time = time;
              resolve(result); //----------->>FailureEndpoint
            }
            break;
          case 'publishmessage':
            result = await brokerFeatureMasterImport.publishMessage(defaultConfig.admin.domain, username, password, productCode, exchange, options);
            output = result.result;
            end = new Date().getTime();
            time = end - start;
            if (result.isSuccess) {
              resolve(new featureSchema(true, false, false, output, 'WrapperMethods', time, 'Null', 'Null')); //----->> SuccessEndPoint
            } else {
              result.type = 'WrapperMethods';
              result.time = time;
              resolve(result); //----------->>FailureEndpoint
            }
            break;
          default:
            end = new Date().getTime();
            time = end - start;
            resolve(new featureSchema(false, false, true, [], 'WrapperMethods', time, 'Null', 'Please input a valid action to perform')); //----->> LimitaionEndPoint
        }
      }
    } catch (e) {
      end = new Date().getTime();
      time = end - start;
      console.log(e);
      resolve(new featureSchema(false, true, false, [], 'WrapperMethods', time, e.toString(), 'Null')); //----->> ErrorEndPoint
    }
  });
}
module.exports.wrapperFeatureRouter = wrapperFeature;
module.exports.processActions = processActions;
module.exports.getActions=getActions;

async function decryptToken(body) {
  return new Promise(async(resolve, reject) => {
    let out = {};
    try {
      if (body.token != undefined) {
        out = await keyVault.getSecret(body.token);
      }
      out = out ? JSON.parse(cryptr.decrypt(out)) : {};
      resolve(out);
    } catch (err) { resolve({}) }
  });
}
