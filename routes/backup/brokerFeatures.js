'use strict';

var axios = require('axios');
var btoa = require('btoa');

var brokerFeature = {};
brokerFeature.admin = {};
brokerFeature.user = {};
var createFeature = [bindExchangetoQueue, getMessageFromQueue, actions, publish];
var readFeature = [];
var updateFeature = [putExchange, putQueue, putvHost, putUser, putUservHostPermission];
var deleteFeature = [deleteExchange, deleteQueue, deletevHost, deleteUser, deleteUservHostPermission];

var allFeatures = {};
    allFeatures.aboutBroker                     = aboutBroker;
    allFeatures.aboutUser                       = aboutUser;
    allFeatures.listVhosts                      = listVhosts;
    allFeatures.listQueue                       = listQueue;
    allFeatures.isVhostLive                     = isVhostLive;
    allFeatures.listUsers                       = listUsers;
    allFeatures.listConsumers                   = listConsumers;
    allFeatures.listExchanges                   = listExchanges;
    allFeatures.listQueueBindings               = listQueueBindings;
    allFeatures.listExchangeBindingsSource      = listExchangeBindingsSource;
    allFeatures.listExchangeBindingsDestination = listExchangeBindingsDestination;

// allFeatures.aboutBroker('issez-s155:15672','test','test').then(x=>{
//     console.log(x);
// }).catch(e=>{
//     console.log(e);
// })

function featureSchema(success, error, result, type, time, errMsg, limitation, limitationMsg) {
    this.isSuccess = (success == undefined) ? false : success;
    this.isError = (error == undefined) ? false : error;
    this.isLimitation = (limitation == undefined) ? true : limitation;
    this.result = result || [];
    this.featureType = type || 'basic';
    this.timeTaken_ms = time || 0;
    this.errMsg = errMsg || 'Null';
    this.limitationMsg = limitationMsg || 'Null';
}

//API - REFERENCE => [[ https://pulse.mozilla.org/api/ || https://pulse.mozilla.org/doc/stats.html || https://www.tutlane.com/tutorial/rabbitmq/]] 

function listVhosts(domain, u_name, u_password) { //---API Endpoint - /api/vhosts/ [GET]

    let start = new Date().getTime(); let end; let time;

    return new Promise((resolve, reject) => {
        try {
            let config = {
                method: 'get',
                // url: 'http://localhost:15672/api/vhosts',
                url: `http://${domain}/api/vhosts/`,
                headers: {
                    'Authorization': `Basic ${btoa(`${u_name}:${u_password}`)}`
                }
            };

            axios(config).then(function (response) {
                //console.log(JSON.stringify(response.data));
                end = new Date().getTime();
                time = end - start;
                resolve(new featureSchema(true, false, response.data, 'listVhosts', time, 'Null')); //----->> SuccessEndPoint
            }).catch(function (err2) {
                //console.log(Object.keys(err2));
                let resp = (err2.response == undefined) ? [] : err2.response.data;
                end = new Date().getTime();
                time = end - start;
                resolve(new featureSchema(false, true, resp, 'listVhosts', time, err2.toString())); //----->> ErrorEndPoint
            });

        } catch (err1) {
            end = new Date().getTime();
            time = end - start;
            console.log(err1);
            resolve(new featureSchema(false, true, [], 'listVhosts', time, err1.toString())); //----->> ErrorEndPoint
        }
    });

}
//listVhosts('localhost:15672','guest','guest').then((msg)=>{ console.log(msg); });


function listQueue(domain, vhost, u_name, u_password) { //---API Endpoint - /api/queues/${vhost} [GET]

    let start = new Date().getTime(); let end; let time;

    return new Promise((resolve, reject) => {
        try {
            let config = {
                method: 'get',
                // url: 'http://localhost:15672/api/queues/harry_host',
                url: `http://${domain}/api/queues/${vhost}`,
                headers: {
                    'Authorization': `Basic ${btoa(`${u_name}:${u_password}`)}`
                }
            };

            axios(config).then(function (response) {
                //console.log(JSON.stringify(response.data));
                end = new Date().getTime();
                time = end - start;
                resolve(new featureSchema(true, false, response.data, 'listQueue', time, 'Null')); //----->> SuccessEndPoint
            }).catch(function (err2) {
                //console.log(Object.keys(err2));
                let resp = (err2.response == undefined) ? [] : err2.response.data;
                end = new Date().getTime();
                time = end - start;
                resolve(new featureSchema(false, true, resp, 'listQueue', time, err2.toString())); //----->> ErrorEndPoint
            });

        } catch (err1) {
            end = new Date().getTime();
            time = end - start;
            console.log(err1);
            resolve(new featureSchema(false, true, [], 'listQueue', time, err1.toString())); //----->> ErrorEndPoint
        }
    });

}
// listQueue('localhost:15672','harry_host','guest','guest').then((msg)=>{ console.log(msg); });


function isVhostLive(domain, vhost, u_name, u_password) { //---API Endpoint - /api/aliveness-test/${vhost} [GET]

    let start = new Date().getTime(); let end; let time;

    return new Promise((resolve, reject) => {
        try {
            let config = {
                method: 'get',
                // url: 'http://localhost:15672/api/aliveness-test/harry_host',
                url: `http://${domain}/api/aliveness-test/${vhost}`,
                headers: {
                    'Authorization': `Basic ${btoa(`${u_name}:${u_password}`)}`
                }
            };

            axios(config).then(function (response) {
                //console.log(JSON.stringify(response.data));
                end = new Date().getTime();
                time = end - start;
                resolve(new featureSchema(true, false, response.data, 'isVhostLive', time, 'Null')); //----->> SuccessEndPoint
            }).catch(function (err2) {
                //console.log(Object.keys(err2));
                let resp = (err2.response == undefined) ? [] : err2.response.data;
                end = new Date().getTime();
                time = end - start;
                resolve(new featureSchema(false, true, resp, 'isVhostLive', time, err2.toString())); //----->> ErrorEndPoint
            });

        } catch (err1) {
            end = new Date().getTime();
            time = end - start;
            console.log(err1);
            resolve(new featureSchema(false, true, [], 'isVhostLive', time, error.toString())); //----->> ErrorEndPoint
        }
    });

}
//isVhostLive('localhost:15672','harry_host','guest','guest').then((msg)=>{ console.log(msg); });

function listUsers(domain, u_name, u_password) { //---API Endpoint - /api/users [GET]

    let start = new Date().getTime(); let end; let time;

    return new Promise((resolve, reject) => {
        try {
            let config = {
                method: 'get',
                // url: 'http://localhost:15672/api/users',
                url: `http://${domain}/api/users/`,
                headers: {
                    'Authorization': `Basic ${btoa(`${u_name}:${u_password}`)}`
                }
            };

            axios(config).then(function (response) {
                //console.log(JSON.stringify(response.data));
                end = new Date().getTime();
                time = end - start;
                resolve(new featureSchema(true, false, response.data, 'listUsers', time, 'Null')); //----->> SuccessEndPoint
            }).catch(function (err2) {
                //console.log(Object.keys(err2));
                let resp = (err2.response == undefined) ? [] : err2.response.data;
                end = new Date().getTime();
                time = end - start;
                resolve(new featureSchema(false, true, resp, 'listUsers', time, err2.toString())); //----->> ErrorEndPoint
            });

        } catch (err1) {
            end = new Date().getTime();
            time = end - start;
            console.log(err1);
            resolve(new featureSchema(false, true, [], 'listUsers', time, err1.toString())); //----->> ErrorEndPoint
        }
    });

}
//listUsers('localhost:15672', 'guest','guest').then((msg)=>{ console.log(msg); });

function listConsumers(domain, vhost, u_name, u_password) { //---API Endpoint - /api/consumers/${vhost} [GET]

    let start = new Date().getTime(); let end; let time;

    return new Promise((resolve, reject) => {
        try {
            let config = {
                method: 'get',
                // url: 'http://localhost:15672/api/consumers/harry_host',
                url: `http://${domain}/api/consumers/${vhost}`,
                headers: {
                    'Authorization': `Basic ${btoa(`${u_name}:${u_password}`)}`
                }
            };

            axios(config).then(function (response) {
                //console.log(JSON.stringify(response.data));
                end = new Date().getTime();
                time = end - start;
                resolve(new featureSchema(true, false, response.data, 'listConsumers', time, 'Null')); //----->> SuccessEndPoint
            }).catch(function (err2) {
                //console.log(Object.keys(err2));
                let resp = (err2.response == undefined) ? [] : err2.response.data;
                end = new Date().getTime();
                time = end - start;
                resolve(new featureSchema(false, true, resp, 'listConsumers', time, err2.toString())); //----->> ErrorEndPoint
            });

        } catch (err1) {
            end = new Date().getTime();
            time = end - start;
            console.log(err1);
            resolve(new featureSchema(false, true, [], 'listConsumers', time, err1.toString())); //----->> ErrorEndPoint
        }
    });

}
//listConsumers('localhost:15672', 'harry_host','guest','guest').then((msg)=>{ console.log(msg); });

function listExchanges(domain, vhost, u_name, u_password) { //---API Endpoint - /api/exchanges/${vhost} [GET]

    let start = new Date().getTime(); let end; let time;

    return new Promise((resolve, reject) => {
        try {
            let config = {
                method: 'get',
                // url: 'http://localhost:15672/api/exchanges/harry_host',
                url: `http://${domain}/api/exchanges/${vhost}`,
                headers: {
                    'Authorization': `Basic ${btoa(`${u_name}:${u_password}`)}`
                }
            };

            axios(config).then(function (response) {
                //console.log(JSON.stringify(response.data));
                end = new Date().getTime();
                time = end - start;
                resolve(new featureSchema(true, false, response.data, 'listExchanges', time, 'Null')); //----->> SuccessEndPoint
            }).catch(function (err2) {
                //console.log(Object.keys(err2));
                let resp = (err2.response == undefined) ? [] : err2.response.data;
                end = new Date().getTime();
                time = end - start;
                resolve(new featureSchema(false, true, resp, 'listExchanges', time, err2.toString())); //----->> ErrorEndPoint
            });

        } catch (err1) {
            end = new Date().getTime();
            time = end - start;
            console.log(err1);
            resolve(new featureSchema(false, true, [], 'listExchanges', time, err1.toString())); //----->> ErrorEndPoint
        }
    });

}
//listExchanges('localhost:15672', 'harry_host','guest','guest').then((msg)=>{ console.log(msg); });



function listQueueBindings(domain, vhost, queue, u_name, u_password) { //---API Endpoint - /api/queues/${vhost}/${queue}/bindings [GET]

    let start = new Date().getTime(); let end; let time;

    return new Promise((resolve, reject) => {
        try {
            let config = {
                method: 'get',
                // url: 'http://localhost:15672/api/queues/harry_host/engine_2_publish/bindings',
                url: `http://${domain}/api/queues/${vhost}/${queue}/bindings`,
                headers: {
                    'Authorization': `Basic ${btoa(`${u_name}:${u_password}`)}`
                }
            };

            axios(config).then(function (response) {
                //console.log(JSON.stringify(response.data));
                end = new Date().getTime();
                time = end - start;
                resolve(new featureSchema(true, false, response.data, 'listQueueBindings', time, 'Null')); //----->> SuccessEndPoint
            }).catch(function (err2) {
                //console.log(Object.keys(err2));
                let resp = (err2.response == undefined) ? [] : err2.response.data;
                end = new Date().getTime();
                time = end - start;
                resolve(new featureSchema(false, true, resp, 'listQueueBindings', time, err2.toString())); //----->> ErrorEndPoint
            });

        } catch (err1) {
            end = new Date().getTime();
            time = end - start;
            console.log(err1);
            resolve(new featureSchema(false, true, [], 'listQueueBindings', time, err1.toString())); //----->> ErrorEndPoint
        }
    });

}
//listQueueBindings('localhost:15672', 'harry_host', 'engine_2_publish', 'guest','guest').then((msg)=>{ console.log(msg); });

function listExchangeBindingsSource(domain, vhost, exchange, u_name, u_password) { //---API Endpoint - /api/exchanges/${vhost}/${exchange}/bindings/source [GET]

    let start = new Date().getTime(); let end; let time;

    return new Promise((resolve, reject) => {
        try {
            let config = {
                method: 'get',
                // url: 'http://localhost:15672/api/exchanges/harry_host/harry_fanout/bindings/source',
                url: `http://${domain}/api/exchanges/${vhost}/${exchange}/bindings/source`,
                headers: {
                    'Authorization': `Basic ${btoa(`${u_name}:${u_password}`)}`
                }
            };

            axios(config).then(function (response) {
                //console.log(JSON.stringify(response.data));
                end = new Date().getTime();
                time = end - start;
                resolve(new featureSchema(true, false, response.data, 'listExchangeBindingsSource', time, 'Null')); //----->> SuccessEndPoint
            }).catch(function (err2) {
                //console.log(Object.keys(err2));
                let resp = (err2.response == undefined) ? [] : err2.response.data;
                end = new Date().getTime();
                time = end - start;
                resolve(new featureSchema(false, true, resp, 'listExchangeBindingsSource', time, err2.toString())); //----->> ErrorEndPoint
            });

        } catch (err1) {
            end = new Date().getTime();
            time = end - start;
            console.log(err1);
            resolve(new featureSchema(false, true, [], 'listExchangeBindingsSource', time, err1.toString())); //----->> ErrorEndPoint
        }
    });

}
//listExchangeBindingsSource('localhost:15672', 'harry_host', 'harry_direct', 'guest','guest').then((msg)=>{ console.log(msg); });

function listExchangeBindingsDestination(domain, vhost, exchange, u_name, u_password) { //---API Endpoint - /api/exchanges/${vhost}/${exchange}/bindings/destination [GET]

    let start = new Date().getTime(); let end; let time;

    return new Promise((resolve, reject) => {
        try {
            let config = {
                method: 'get',
                // url: 'http://localhost:15672/api/exchanges/harry_host/harry_fanout/bindings/destination',
                url: `http://${domain}/api/exchanges/${vhost}/${exchange}/bindings/destination`,
                headers: {
                    'Authorization': `Basic ${btoa(`${u_name}:${u_password}`)}`
                }
            };

            axios(config).then(function (response) {
                //console.log(JSON.stringify(response.data));
                end = new Date().getTime();
                time = end - start;
                resolve(new featureSchema(true, false, response.data, 'listExchangeBindingsDestination', time, 'Null')); //----->> SuccessEndPoint
            }).catch(function (err2) {
                //console.log(Object.keys(err2));
                let resp = (err2.response == undefined) ? [] : err2.response.data;
                end = new Date().getTime();
                time = end - start;
                resolve(new featureSchema(false, true, resp, 'listExchangeBindingsDestination', time, err2.toString())); //----->> ErrorEndPoint
            });

        } catch (err1) {
            end = new Date().getTime();
            time = end - start;
            console.log(err1);
            resolve(new featureSchema(false, true, [], 'listExchangeBindingsDestination', time, err1.toString())); //----->> ErrorEndPoint
        }
    });

}
//listExchangeBindingsDestination('localhost:15672', 'harry_host', 'harry_fanout', 'guest','guest').then((msg)=>{ console.log(msg); });

function aboutBroker(domain, u_name, u_password) { //---API Endpoint - /api/overview [GET]

    let start = new Date().getTime(); let end; let time;

    return new Promise((resolve, reject) => {
        try {
            let config = {
                method: 'get',
                // url: 'http://localhost:15672/api/overview',
                url: `http://${domain}/api/overview/`,
                headers: {
                    'Authorization': `Basic ${btoa(`${u_name}:${u_password}`)}`
                }
            };

            axios(config).then(function (response) {
                console.log(response);
                //console.log(JSON.stringify(response.data));
                end = new Date().getTime();
                time = end - start;
                resolve(new featureSchema(true, false, response.data, 'aboutBroker', time, 'Null')); //----->> SuccessEndPoint
            }).catch(function (err2) {
                //console.log(Object.keys(err2)); //console.log(err2);
                let resp = (err2.response == undefined) ? [] : err2.response.data;
                end = new Date().getTime();
                time = end - start;
                resolve(new featureSchema(false, true, resp, 'aboutBroker', time, err2.toString())); //----->> ErrorEndPoint
            });

        } catch (err1) {
            end = new Date().getTime();
            time = end - start;
            console.log(err1);
            resolve(new featureSchema(false, true, [], 'aboutBroker', time, err1.toString())); //----->> ErrorEndPoint
        }
    });

}

function aboutUser(domain, u_name, u_password) { //---API Endpoint - /api/whoami [GET]

    let start = new Date().getTime(); let end; let time;

    return new Promise((resolve, reject) => {
        try {
            let config = {
                method: 'get',
                // url: 'http://localhost:15672/api/whoami',
                url: `http://${domain}/api/whoami/`,
                headers: {
                    'Authorization': `Basic ${btoa(`${u_name}:${u_password}`)}`
                }
            };

            axios(config).then(function (response) {
                //console.log(response);
                //console.log(JSON.stringify(response.data));
                end = new Date().getTime();
                time = end - start;
                resolve(new featureSchema(true, false, response.data, 'aboutUser', time, 'Null')); //----->> SuccessEndPoint
            }).catch(function (err2) {
                //console.log(Object.keys(err2)); //console.log(err2);
                let resp = (err2.response == undefined) ? [] : err2.response.data;
                end = new Date().getTime();
                time = end - start;
                resolve(new featureSchema(false, true, resp, 'aboutUser', time, err2.toString())); //----->> ErrorEndPoint
            });

        } catch (err1) {
            end = new Date().getTime();
            time = end - start;
            console.log(err1);
            resolve(new featureSchema(false, true, [], 'aboutUser', time, err1.toString())); //----->> ErrorEndPoint
        }
    });

}
//aboutUser('localhost:15672','guest','guest').then((msg)=>{ console.log(msg); });

function bindExchangetoQueue(domain, u_name, u_password, vhost, exchange, queue, options) { //---API Endpoint - /api/bindings/${vhost}/e/${exchange}/q/${queue} [POST]
    let start = new Date().getTime(); let end; let time;
    return new Promise(async (resolve, reject) => {
        try {
            let vhostList = await listVhosts(domain, u_name, u_password);
            if (vhostList.isError == true) { throw vhostList }
            let vhosts = [];
            vhostList.result.forEach(element => {
                vhosts.push(element.name)
            });
            if (!vhosts.includes(vhost)) {
                end = new Date().getTime();
                time = end - start;
                resolve(new featureSchema(false, false, [], 'BindExchangetoQueue', time, null, true, "No such vhost available."));
                return;
            }
            let queueList = await listQueue(domain, vhost, u_name, u_password);
            if (queueList.isError == true) { throw queueList }
            let queues = [];
            queueList.result.forEach(element => {
                queues.push(element.name)
            });
            if (!queues.includes(queue)) {
                end = new Date().getTime();
                time = end - start;
                resolve(new featureSchema(false, false, [], 'BindExchangetoQueue', time, null, true, "No such queue available."));
                return;
            }
            let exchangeList = await listExchanges(domain, vhost, u_name, u_password);
            if (exchangeList.isError == true) { throw queueList }
            let exchanges = [];
            exchangeList.result.forEach(element => {
                exchanges.push(element.name)
            });
            if (!exchanges.includes(exchange)) {
                end = new Date().getTime();
                time = end - start;
                resolve(new featureSchema(false, false, [], 'BindExchangetoQueue', time, null, true, "No such exchange available."));
                return;
            }
            let config = {
                method: 'post',
                url: `http://${domain}/api/bindings/${vhost}/e/${exchange}/q/${queue}`,
                headers: {
                    'Authorization': `Basic ${btoa(`${u_name}:${u_password}`)}`,
                    'Content-Type': 'application/json'
                },
                data: options
            };
            axios(config).then(function (response) {
                end = new Date().getTime();
                time = end - start;
                resolve(new featureSchema(true, false, response.data, 'BindExchangetoQueue', time, 'Null')); //----->> SuccessEndPoint
            }).catch(function (err2) {
                let resp = (err2.response == undefined) ? [] : err2.response.data;
                end = new Date().getTime();
                time = end - start;
                resolve(new featureSchema(false, true, resp, 'BindExchangetoQueue', time, err2.toString())); //----->> ErrorEndPoint
            });
        } catch (err1) {
            end = new Date().getTime();
            time = end - start;
            console.log(err1);
            resolve(new featureSchema(false, true, [], 'BindExchangetoQueue', time, err1.toString())); //----->> ErrorEndPoint
        }
    });
}
// bindExchangetoQueue('issez-s155:15672','test','test','iAuthor','iAuthor','iAuthor',{"routing_key":"my_routing_key","arguments":{}}).then(x=>{
//     console.log(x);
// }).catch(e=>{
//     console.log(e);
// })

function getMessageFromQueue(domain, u_name, u_password, vhost, queue, options) { //---API Endpoint - /api/${queues}/${vhost}/name/get [POST]
    let start = new Date().getTime(); let end; let time;
    return new Promise(async (resolve, reject) => {
        try {
            let ackModeList = ["ack_requeue_true",
                "ack_requeue_false",
                "reject_requeue_true",
                "reject_requeue_false"]
            if (options.ackmode == undefined || options.ackmode == "") {
                end = new Date().getTime();
                time = end - start;
                resolve(new featureSchema(false, false, [], 'getMessageFromQueue', time, null, true, `ackmode should be provided (${ackModeList.join(",")})`));
                return;
            }
            else if (!ackModeList.includes(options.ackmode)) {
                end = new Date().getTime();
                time = end - start;
                resolve(new featureSchema(false, false, [], 'getMessageFromQueue', time, null, true, `allowed ackmode are ${ackModeList.join(",")}`));
                return;
            }
            options.count = options.count ? options.count : 1
            options.encoding = options.encoding ? options.encoding : "auto";
            let vhostList = await listVhosts(domain, u_name, u_password);
            if (vhostList.isError == true) { throw vhostList }
            let vhosts = [];
            vhostList.result.forEach(element => {
                vhosts.push(element.name)
            });
            if (!vhosts.includes(vhost)) {
                end = new Date().getTime();
                time = end - start;
                resolve(new featureSchema(false, false, [], 'getMessageFromQueue', time, null, true, "No such vhost available."));
                return;
            }
            let queueList = await listQueue(domain, vhost, u_name, u_password);
            if (queueList.isError == true) { throw queueList }
            let queues = [];
            queueList.result.forEach(element => {
                queues.push(element.name)
            });
            if (!queues.includes(queue)) {
                end = new Date().getTime();
                time = end - start;
                resolve(new featureSchema(false, false, [], 'getMessageFromQueue', time, null, true, "No such queue available."));
                return;
            }
            let config = {
                method: 'post',
                url: `http://${domain}/api/queues/${vhost}/${queue}/get`,
                headers: {
                    'Authorization': `Basic ${btoa(`${u_name}:${u_password}`)}`,
                    'Content-Type': 'application/json'
                },
                data: options
            };
            axios(config).then(function (response) {
                end = new Date().getTime();
                time = end - start;
                resolve(new featureSchema(true, false, response.data, 'getMessageFromQueue', time, 'Null')); //----->> SuccessEndPoint
            }).catch(function (err2) {
                let resp = (err2.response == undefined) ? [] : err2.response.data;
                end = new Date().getTime();
                time = end - start;
                resolve(new featureSchema(false, true, resp, 'getMessageFromQueue', time, err2.toString())); //----->> ErrorEndPoint
            });
        } catch (err1) {
            end = new Date().getTime();
            time = end - start;
            console.log(err1);
            resolve(new featureSchema(false, true, [], 'getMessageFromQueue', time, err1.toString())); //----->> ErrorEndPoint
        }
    });
}
// getMessageFromQueue('issez-s155:15672','test','test','iAuthor','iAuthor',{
//     "count": 5,"encoding": "auto","ackmode":"ack_requeue_true"
// }).then(x=>{
//     console.log(x);
// }).catch(e=>{
//     console.log(e);
// })

function actions(domain, u_name, u_password, vhost, queue, options) { //---API Endpoint - /api/queues/${vhost}/${queue}/actions [POST]
    let start = new Date().getTime(); let end; let time;
    return new Promise(async (resolve, reject) => {
        try {
            let actionList = ["sync", "cancel_sync"];
            if (options.action == undefined || options.action == "") { 
                end = new Date().getTime();
                time = end - start;
                resolve(new featureSchema(false, false, [], 'actions', time, null, true, `action should be provided (${actionList.join(",")})`));
                return;
            }
            else if (!actionList.includes(options.action)) { 
                end = new Date().getTime();
                time = end - start;
                resolve(new featureSchema(false, false, [], 'actions', time, null, true, `allowed action are ${actionList.join(",")}`));
                return;
            }
            let vhostList = await listVhosts(domain, u_name, u_password);
            if (vhostList.isError == true) { throw vhostList }
            let vhosts = [];
            vhostList.result.forEach(element => {
                vhosts.push(element.name)
            });
            if (!vhosts.includes(vhost)) {
                end = new Date().getTime();
                time = end - start;
                resolve(new featureSchema(false, false, [], 'actions', time, null, true, "No such vhost available."));
                return;
            }
            let queueList = await listQueue(domain, vhost, u_name, u_password);
            if (queueList.isError == true) { throw queueList }
            let queues = [];
            queueList.result.forEach(element => {
                queues.push(element.name)
            });
            if (!queues.includes(queue)) {
                end = new Date().getTime();
                time = end - start;
                resolve(new featureSchema(false, false, [], 'actions', time, null, true, "No such queue available."));
                return;
            }
            let config = {
                method: 'post',
                url: `http://${domain}/api/queues/${vhost}/${queue}/actions`,
                headers: {
                    'Authorization': `Basic ${btoa(`${u_name}:${u_password}`)}`,
                    'Content-Type': 'application/json'
                },
                data: options
            };
            axios(config).then(function (response) {
                end = new Date().getTime();
                time = end - start;
                resolve(new featureSchema(true, false, response.data, 'actions', time, 'Null')); //----->> SuccessEndPoint
            }).catch(function (err2) {
                let resp = (err2.response == undefined) ? [] : err2.response.data;
                end = new Date().getTime();
                time = end - start;
                resolve(new featureSchema(false, true, resp, 'actions', time, err2.toString())); //----->> ErrorEndPoint
            });
        } catch (err1) {
            end = new Date().getTime();
            time = end - start;
            console.log(err1);
            resolve(new featureSchema(false, true, [], 'actions', time, err1.toString())); //----->> ErrorEndPoint
        }
    });
}
// actions('issez-s155:15672','test','test','iAuthor','iAuthor',{
//     "action": "sync"
// }).then(x=>{
//     console.log(x);
// }).catch(e=>{
//     console.log(e);
// })

function publish(domain, u_name, u_password, vhost, exchange, options) { //---API Endpoint - /api/exchanges/${vhost}/${exchange}/publish [POST]
    let start = new Date().getTime(); let end; let time;
    return new Promise(async (resolve, reject) => {
        try {
            let vhostList = await listVhosts(domain, u_name, u_password);
            if (vhostList.isError == true) { throw vhostList }
            let vhosts = [];
            vhostList.result.forEach(element => {
                vhosts.push(element.name)
            });
            if (!vhosts.includes(vhost)) {
                end = new Date().getTime();
                time = end - start;
                resolve(new featureSchema(false, false, [], 'publish', time, null, true, "No such vhost available."));
                return;
            }
            let exchangeList = await listExchanges(domain, vhost, u_name, u_password);
            if (exchangeList.isError == true) { throw exchangeList }
            let exchanges = [];
            exchangeList.result.forEach(element => {
                exchanges.push(element.name)
            });
            if (!exchanges.includes(exchange)) {
                end = new Date().getTime();
                time = end - start;
                resolve(new featureSchema(false, false, [], 'publish', time, null, true, "No such exchange available."));
                return;
            }
            let config = {
                method: 'post',
                url: `http://${domain}/api/exchanges/${vhost}/${exchange}/publish`,
                headers: {
                    'Authorization': `Basic ${btoa(`${u_name}:${u_password}`)}`,
                    'Content-Type': 'application/json'
                },
                data: options
            };
            axios(config).then(function (response) {
                end = new Date().getTime();
                time = end - start;
                resolve(new featureSchema(true, false, response.data, 'publish', time, 'Null')); //----->> SuccessEndPoint
            }).catch(function (err2) {
                let resp = (err2.response == undefined) ? [] : err2.response.data;
                end = new Date().getTime();
                time = end - start;
                resolve(new featureSchema(false, true, resp, 'publish', time, err2.toString())); //----->> ErrorEndPoint
            });
        } catch (err1) {
            end = new Date().getTime();
            time = end - start;
            console.log(err1);
            resolve(new featureSchema(false, true, [], 'publish', time, err1.toString())); //----->> ErrorEndPoint
        }
    });
}
// publish('issez-s155:15672','test','test','iAuthor','iAuthor',{"properties":{},"routing_key":"my_routing_key","payload":"my body","payload_encoding":"string"}).then(x=>{
//     console.log(x);
// }).catch(e=>{
//     console.log(e);
// })

function putExchange(domain, u_name, u_password, vhost, exchange, options) { //---API Endpoint - /api/exchanges/${vhost}/${exchange} [PUT]
    let start = new Date().getTime(); let end; let time;
    return new Promise(async (resolve, reject) => {
        try {
            let vhostList = await listVhosts(domain, u_name, u_password);
            if (vhostList.isError == true) { throw vhostList }
            let vhosts = [];
            vhostList.result.forEach(element => {
                vhosts.push(element.name)
            });
            if (!vhosts.includes(vhost)) {
                end = new Date().getTime();
                time = end - start;
                resolve(new featureSchema(false, false, [], 'putExchange', time, null, true, "No such vhost available."));
                return;
            }
            let exchangeList = await listExchanges(domain, vhost, u_name, u_password);
            if (exchangeList.isError == true) { throw exchangeList }
            let exchanges = [];
            exchangeList.result.forEach(element => {
                exchanges.push(element.name)
            });
            if (!exchanges.includes(exchange)) {
                end = new Date().getTime();
                time = end - start;
                resolve(new featureSchema(false, false, [], 'putExchange', time, null, true, "No such exchange available."));
                return;
            }
            let config = {
                method: 'put',
                url: `http://${domain}/api/exchanges/${vhost}/${exchange}`,
                headers: {
                    'Authorization': `Basic ${btoa(`${u_name}:${u_password}`)}`,
                    'Content-Type': 'application/json'
                },
                data: options
            };
            axios(config).then(function (response) {
                end = new Date().getTime();
                time = end - start;
                resolve(new featureSchema(true, false, response.data, 'putExchange', time, 'Null')); //----->> SuccessEndPoint
            }).catch(function (err2) {
                let resp = (err2.response == undefined) ? [] : err2.response.data;
                end = new Date().getTime();
                time = end - start;
                resolve(new featureSchema(false, true, resp, 'putExchange', time, err2.toString())); //----->> ErrorEndPoint
            });
        } catch (err1) {
            end = new Date().getTime();
            time = end - start;
            console.log(err1);
            resolve(new featureSchema(false, true, [], 'putExchange', time, err1.toString())); //----->> ErrorEndPoint
        }
    });
}
// putExchange('issez-s155:15672','test','test','iAuthor','iAuthor',{
//     "type": "fanout",
//     "auto_delete": false,
//     "durable": true,
//     "internal": false,
//     "arguments": {}
// }).then(x=>{
//     console.log(x);
// }).catch(e=>{
//     console.log(e);
// })

function deleteExchange(domain, u_name, u_password, vhost, exchange) { //---API Endpoint - /api/exchanges/${vhost}/${exchange} [DELETE]
    let start = new Date().getTime(); let end; let time;
    return new Promise(async (resolve, reject) => {
        try {
            let vhostList = await listVhosts(domain, u_name, u_password);
            if (vhostList.isError == true) { throw vhostList }
            let vhosts = [];
            vhostList.result.forEach(element => {
                vhosts.push(element.name)
            });
            if (!vhosts.includes(vhost)) {
                end = new Date().getTime();
                time = end - start;
                resolve(new featureSchema(false, false, [], 'deleteExchange', time, null, true, "No such vhost available."));
                return;
            }
            let exchangeList = await listExchanges(domain, vhost, u_name, u_password);
            if (exchangeList.isError == true) { throw exchangeList }
            let exchanges = [];
            exchangeList.result.forEach(element => {
                exchanges.push(element.name)
            });
            if (!exchanges.includes(exchange)) {
                end = new Date().getTime();
                time = end - start;
                resolve(new featureSchema(false, false, [], 'deleteExchange', time, null, true, "No such exchange available."));
                return;
            }
            let config = {
                method: 'delete',
                url: `http://${domain}/api/exchanges/${vhost}/${exchange}`,
                headers: {
                    'Authorization': `Basic ${btoa(`${u_name}:${u_password}`)}`,
                    'Content-Type': 'application/json'
                }
            };
            axios(config).then(function (response) {
                end = new Date().getTime();
                time = end - start;
                resolve(new featureSchema(true, false, response.data, 'deleteExchange', time, 'Null')); //----->> SuccessEndPoint
            }).catch(function (err2) {
                let resp = (err2.response == undefined) ? [] : err2.response.data;
                end = new Date().getTime();
                time = end - start;
                resolve(new featureSchema(false, true, resp, 'deleteExchange', time, err2.toString())); //----->> ErrorEndPoint
            });
        } catch (err1) {
            end = new Date().getTime();
            time = end - start;
            console.log(err1);
            resolve(new featureSchema(false, true, [], 'deleteExchange', time, err1.toString())); //----->> ErrorEndPoint
        }
    });
}
// deleteExchange('issez-s155:15672','test','test','iAuthor','iAuthor').then(x=>{
//     console.log(x);
// }).catch(e=>{
//     console.log(e);
// })

function putQueue(domain, u_name, u_password, vhost, queue, options) { //---API Endpoint - /api/queues/${vhost}/${queue} [PUT]
    let start = new Date().getTime(); let end; let time;
    return new Promise(async (resolve, reject) => {
        try {
            let vhostList = await listVhosts(domain, u_name, u_password);
            if (vhostList.isError == true) { throw vhostList }
            let vhosts = [];
            vhostList.result.forEach(element => {
                vhosts.push(element.name)
            });
            if (!vhosts.includes(vhost)) {
                end = new Date().getTime();
                time = end - start;
                resolve(new featureSchema(false, false, [], 'putQueue', time, null, true, "No such vhost available."));
                return;
            }
            let queueList = await listQueue(domain, vhost, u_name, u_password);
            if (queueList.isError == true) { throw queueList }
            let queues = [];
            queueList.result.forEach(element => {
                queues.push(element.name)
            });
            if (!queues.includes(queue)) {
                end = new Date().getTime();
                time = end - start;
                resolve(new featureSchema(false, false, [], 'putQueue', time, null, true, "No such queue available."));
                return;
            }
            let config = {
                method: 'put',
                url: `http://${domain}/api/queues/${vhost}/${queue}`,
                headers: {
                    'Authorization': `Basic ${btoa(`${u_name}:${u_password}`)}`,
                    'Content-Type': 'application/json'
                },
                data: options
            };
            axios(config).then(function (response) {
                end = new Date().getTime();
                time = end - start;
                resolve(new featureSchema(true, false, response.data, 'putQueue', time, 'Null')); //----->> SuccessEndPoint
            }).catch(function (err2) {
                let resp = (err2.response == undefined) ? [] : err2.response.data;
                end = new Date().getTime();
                time = end - start;
                resolve(new featureSchema(false, true, resp, 'putQueue', time, err2.toString())); //----->> ErrorEndPoint
            });
        } catch (err1) {
            end = new Date().getTime();
            time = end - start;
            console.log(err1);
            resolve(new featureSchema(false, true, [], 'putQueue', time, err1.toString())); //----->> ErrorEndPoint
        }
    });
}
// putQueue('issez-s155:15672','test','test','iAuthor','iAuthor',{"auto_delete":false,"durable":true,"arguments":{}}).then(x=>{
//     console.log(x);
// }).catch(e=>{
//     console.log(e);
// })

function deleteQueue(domain, u_name, u_password, vhost, queue, options) { //---API Endpoint - /api/queues/vhost/${queue} [DELETE]
    let start = new Date().getTime(); let end; let time;
    return new Promise(async (resolve, reject) => {
        try {
            let vhostList = await listVhosts(domain, u_name, u_password);
            if (vhostList.isError == true) { throw vhostList }
            let vhosts = [];
            vhostList.result.forEach(element => {
                vhosts.push(element.name)
            });
            if (!vhosts.includes(vhost)) {
                end = new Date().getTime();
                time = end - start;
                resolve(new featureSchema(false, false, [], 'deleteQueue', time, null, true, "No such vhost available."));
                return;
            }
            let queueList = await listQueue(domain, vhost, u_name, u_password);
            if (queueList.isError == true) { throw queueList }
            let queues = [];
            queueList.result.forEach(element => {
                queues.push(element.name)
            });
            if (!queues.includes(queue)) {
                end = new Date().getTime();
                time = end - start;
                resolve(new featureSchema(false, false, [], 'deleteQueue', time, null, true, "No such queue available."));
                return;
            }
            let config = {
                method: 'delete',
                url: `http://${domain}/api/queues/${vhost}/${queue}`,
                headers: {
                    'Authorization': `Basic ${btoa(`${u_name}:${u_password}`)}`,
                    'Content-Type': 'application/json'
                }
            };
            axios(config).then(function (response) {
                end = new Date().getTime();
                time = end - start;
                resolve(new featureSchema(true, false, response.data, 'deleteQueue', time, 'Null')); //----->> SuccessEndPoint
            }).catch(function (err2) {
                let resp = (err2.response == undefined) ? [] : err2.response.data;
                end = new Date().getTime();
                time = end - start;
                resolve(new featureSchema(false, true, resp, 'deleteQueue', time, err2.toString())); //----->> ErrorEndPoint
            });
        } catch (err1) {
            end = new Date().getTime();
            time = end - start;
            console.log(err1);
            resolve(new featureSchema(false, true, [], 'deleteQueue', time, err1.toString())); //----->> ErrorEndPoint
        }
    });
}
// deleteQueue('issez-s155:15672','test','test','iAuthor','iAuthor',{"auto_delete":false,"durable":true,"arguments":{}}).then(x=>{
//     console.log(x);
// }).catch(e=>{
//     console.log(e);
// })

function putvHost(domain, u_name, u_password, vhost, options) { //---API Endpoint - /api/vhosts/${vhost} [PUT]
    let start = new Date().getTime(); let end; let time;
    return new Promise(async (resolve, reject) => {
        try {
            let vhostList = await listVhosts(domain, u_name, u_password);
            if (vhostList.isError == true) { throw vhostList }
            let vhosts = [];
            vhostList.result.forEach(element => {
                vhosts.push(element.name)
            });
            if (!vhosts.includes(vhost)) {
                end = new Date().getTime();
                time = end - start;
                resolve(new featureSchema(false, false, [], 'putvHost', time, null, true, "No such vhost available."));
                return;
            }
            let config = {
                method: 'put',
                url: `http://${domain}/api/vhosts/${vhost}`,
                headers: {
                    'Authorization': `Basic ${btoa(`${u_name}:${u_password}`)}`,
                    'Content-Type': 'application/json'
                },
                data: options
            };
            axios(config).then(function (response) {
                end = new Date().getTime();
                time = end - start;
                resolve(new featureSchema(true, false, response.data, 'putvHost', time, 'Null')); //----->> SuccessEndPoint
            }).catch(function (err2) {
                let resp = (err2.response == undefined) ? [] : err2.response.data;
                end = new Date().getTime();
                time = end - start;
                resolve(new featureSchema(false, true, resp, 'putvHost', time, err2.toString())); //----->> ErrorEndPoint
            });
        } catch (err1) {
            end = new Date().getTime();
            time = end - start;
            console.log(err1);
            resolve(new featureSchema(false, true, [], 'putvHost', time, err1.toString())); //----->> ErrorEndPoint
        }
    });
}
// putvHost('issez-s155:15672','test','test','iAuthor',{"tracing":true}).then(x=>{
//     console.log(x);
// }).catch(e=>{
//     console.log(e);
// })

function deletevHost(domain, u_name, u_password, vhost) { //---API Endpoint - /api/vhosts/${vhost} [DELETE]
    let start = new Date().getTime(); let end; let time;
    return new Promise(async (resolve, reject) => {
        try {
            let vhostList = await listVhosts(domain, u_name, u_password);
            if (vhostList.isError == true) { throw vhostList }
            let vhosts = [];
            vhostList.result.forEach(element => {
                vhosts.push(element.name)
            });
            if (!vhosts.includes(vhost)) {
                end = new Date().getTime();
                time = end - start;
                resolve(new featureSchema(false, false, [], 'deletevHost', time, null, true, "No such vhost available."));
                return;
            }
            let config = {
                method: 'delete',
                url: `http://${domain}/api/vhosts/${vhost}`,
                headers: {
                    'Authorization': `Basic ${btoa(`${u_name}:${u_password}`)}`,
                    'Content-Type': 'application/json'
                }
            };
            axios(config).then(function (response) {
                end = new Date().getTime();
                time = end - start;
                resolve(new featureSchema(true, false, response.data, 'deletevHost', time, 'Null')); //----->> SuccessEndPoint
            }).catch(function (err2) {
                let resp = (err2.response == undefined) ? [] : err2.response.data;
                end = new Date().getTime();
                time = end - start;
                resolve(new featureSchema(false, true, resp, 'deletevHost', time, err2.toString())); //----->> ErrorEndPoint
            });
        } catch (err1) {
            end = new Date().getTime();
            time = end - start;
            console.log(err1);
            resolve(new featureSchema(false, true, [], 'deletevHost', time, err1.toString())); //----->> ErrorEndPoint
        }
    });
}
// deletevHost('issez-s155:15672','test','test','iAuthor').then(x=>{
//     console.log(x);
// }).catch(e=>{
//     console.log(e);
// })

function deleteUser(domain, u_name, u_password, user) { //---API Endpoint - /api/users/${user}	 [DELETE]
    let start = new Date().getTime(); let end; let time;
    return new Promise(async (resolve, reject) => {
        try {
            let userList = await listUsers(domain, u_name, u_password);
            if (userList.isError == true) { throw userList }
            let users = [];
            userList.result.forEach(element => {
                users.push(element.name)
            });
            if (!users.includes(user)) {
                end = new Date().getTime();
                time = end - start;
                resolve(new featureSchema(false, false, [], 'deleteUser', time, null, true, "No such user available."));
                return;
            }
            let config = {
                method: 'delete',
                url: `http://${domain}/api/users/${user}`,
                headers: {
                    'Authorization': `Basic ${btoa(`${u_name}:${u_password}`)}`,
                    'Content-Type': 'application/json'
                }
            };
            axios(config).then(function (response) {
                end = new Date().getTime();
                time = end - start;
                resolve(new featureSchema(true, false, response.data, 'deleteUser', time, 'Null')); //----->> SuccessEndPoint
            }).catch(function (err2) {
                let resp = (err2.response == undefined) ? [] : err2.response.data;
                end = new Date().getTime();
                time = end - start;
                resolve(new featureSchema(false, true, resp, 'deleteUser', time, err2.toString())); //----->> ErrorEndPoint
            });
        } catch (err1) {
            end = new Date().getTime();
            time = end - start;
            console.log(err1);
            resolve(new featureSchema(false, true, [], 'deleteUser', time, err1.toString())); //----->> ErrorEndPoint
        }
    });
}
// deleteUser('issez-s155:15672','test','test','test1').then(x=>{
//     console.log(x);
// }).catch(e=>{
//     console.log(e);
// })

function putUser(domain, u_name, u_password, user, options) { //---API Endpoint - /api/users/${user}	 [PUT]
    let start = new Date().getTime(); let end; let time;
    return new Promise(async (resolve, reject) => {
        try {
            let userList = await listUsers(domain, u_name, u_password);
            if (userList.isError == true) { throw userList }
            let users = [];
            userList.result.forEach(element => {
                users.push(element.name)
            });
            if (!users.includes(user)) {
                end = new Date().getTime();
                time = end - start;
                resolve(new featureSchema(false, false, [], 'putUser', time, null, true, "No such user available."));
                return;
            }
            let config = {
                method: 'put',
                url: `http://${domain}/api/users/${user}`,
                headers: {
                    'Authorization': `Basic ${btoa(`${u_name}:${u_password}`)}`,
                    'Content-Type': 'application/json'
                },
                data: options
            };
            axios(config).then(function (response) {
                end = new Date().getTime();
                time = end - start;
                resolve(new featureSchema(true, false, response.data, 'putUser', time, 'Null')); //----->> SuccessEndPoint
            }).catch(function (err2) {
                let resp = (err2.response == undefined) ? [] : err2.response.data;
                end = new Date().getTime();
                time = end - start;
                resolve(new featureSchema(false, true, resp, 'putUser', time, err2.toString())); //----->> ErrorEndPoint
            });
        } catch (err1) {
            end = new Date().getTime();
            time = end - start;
            console.log(err1);
            resolve(new featureSchema(false, true, [], 'putUser', time, err1.toString())); //----->> ErrorEndPoint
        }
    });
}
// putUser('issez-s155:15672','test','test','test1',{"password":"secret","tags":"administrator"}).then(x=>{
//     console.log(x);
// }).catch(e=>{
//     console.log(e);
// })

function putUservHostPermission(domain, u_name, u_password, vhost, user, options) { //---API Endpoint - /api/permissions/${vhost}/${user} [PUT]
    let start = new Date().getTime(); let end; let time;
    return new Promise(async (resolve, reject) => {
        try {
            let userList = await listUsers(domain, u_name, u_password);
            if (userList.isError == true) { throw userList }
            let users = [];
            userList.result.forEach(element => {
                users.push(element.name)
            });
            if (!users.includes(user)) {
                end = new Date().getTime();
                time = end - start;
                resolve(new featureSchema(false, false, [], 'putUservHostPermission', time, null, true, "No such user available."));
                return;
            }
            let vhostList = await listVhosts(domain, u_name, u_password);
            if (vhostList.isError == true) { throw vhostList }
            let vhosts = [];
            vhostList.result.forEach(element => {
                vhosts.push(element.name)
            });
            if (!vhosts.includes(vhost)) {
                end = new Date().getTime();
                time = end - start;
                resolve(new featureSchema(false, false, [], 'putUservHostPermission', time, null, true, "No such vhost available."));
                return;
            }
            let config = {
                method: 'put',
                url: `http://${domain}/api/permissions/${vhost}/${user}`,
                headers: {
                    'Authorization': `Basic ${btoa(`${u_name}:${u_password}`)}`,
                    'Content-Type': 'application/json'
                },
                data: options
            };
            axios(config).then(function (response) {
                end = new Date().getTime();
                time = end - start;
                resolve(new featureSchema(true, false, response.data, 'putUservHostPermission', time, 'Null')); //----->> SuccessEndPoint
            }).catch(function (err2) {
                let resp = (err2.response == undefined) ? [] : err2.response.data;
                end = new Date().getTime();
                time = end - start;
                resolve(new featureSchema(false, true, resp, 'putUservHostPermission', time, err2.toString())); //----->> ErrorEndPoint
            });
        } catch (err1) {
            end = new Date().getTime();
            time = end - start;
            console.log(err1);
            resolve(new featureSchema(false, true, [], 'putUservHostPermission', time, err1.toString())); //----->> ErrorEndPoint
        }
    });
}
// putUservHostPermission('issez-s155:15672','test','test','iAuthor','test1',{"configure":".*","write":".*","read":".*"}).then(x=>{
//     console.log(x);
// }).catch(e=>{
//     console.log(e);
// })

function deleteUservHostPermission(domain, u_name, u_password, vhost, user) { //---API Endpoint - /api/permissions/${vhost}/${user}	 [DELETE]
    let start = new Date().getTime(); let end; let time;
    return new Promise(async (resolve, reject) => {
        try {
            let userList = await listUsers(domain, u_name, u_password);
            if (userList.isError == true) { throw userList }
            let users = [];
            userList.result.forEach(element => {
                users.push(element.name)
            });
            if (!users.includes(user)) {
                end = new Date().getTime();
                time = end - start;
                resolve(new featureSchema(false, false, [], 'deleteUservHostPermission', time, null, true, "No such user available.")); //----->> ErrorEndPoint
                return;
            }
            let vhostList = await listVhosts(domain, u_name, u_password);
            if (vhostList.isError == true) { throw vhostList }
            let vhosts = [];
            vhostList.result.forEach(element => {
                vhosts.push(element.name)
            });
            if (!vhosts.includes(vhost)) {
                end = new Date().getTime();
                time = end - start;
                resolve(new featureSchema(false, false, [], 'deleteUservHostPermission', time, null, true, "No such vhost available.")); //----->> ErrorEndPoint
                return;
            }
            let config = {
                method: 'delete',
                url: `http://${domain}/api/permissions/${vhost}/${user}`,
                headers: {
                    'Authorization': `Basic ${btoa(`${u_name}:${u_password}`)}`,
                    'Content-Type': 'application/json'
                }
            };
            axios(config).then(function (response) {
                end = new Date().getTime();
                time = end - start;
                resolve(new featureSchema(true, false, response.data, 'deleteUservHostPermission', time, 'Null')); //----->> SuccessEndPoint
            }).catch(function (err2) {
                let resp = (err2.response == undefined) ? [] : err2.response.data;
                end = new Date().getTime();
                time = end - start;
                resolve(new featureSchema(false, true, resp, 'deleteUservHostPermission', time, err2.toString())); //----->> ErrorEndPoint
            });
        } catch (err1) {
            end = new Date().getTime();
            time = end - start;
            console.log(err1);
            resolve(new featureSchema(false, true, [], 'deleteUservHostPermission', time, err1.toString())); //----->> ErrorEndPoint
        }
    });
}
// deleteUservHostPermission('issez-s155:15672','test','test','iAuthor','test1').then(x=>{
//     console.log(x);
// }).catch(e=>{
//     console.log(e);
// })

function deleteQueueData(domain, u_name, u_password, vhost, queue) { //---API Endpoint - /api/queues/${vhost}/${queue}/contents [DELETE]
    let start = new Date().getTime(); let end; let time;
    return new Promise(async (resolve, reject) => {
        try {
                let vhostList = await listVhosts(domain, u_name, u_password);
                if (vhostList.isError == true) { throw vhostList }
                let vhosts = [];
                vhostList.result.forEach(element => {
                    vhosts.push(element.name)
                });
                if (!vhosts.includes(vhost)) {
                    end = new Date().getTime();
                    time = end - start;
                    resolve(new featureSchema(false, false, [], 'deleteQueueData', time, null, true, "No such vhost available."));
                    return;
                }
                let queueList = await listQueue(domain, vhost, u_name, u_password);
                if (queueList.isError == true) { throw queueList }
                let queues = [];
                queueList.result.forEach(element => {
                    queues.push(element.name)
                });
                if (!queues.includes(queue)) {
                    end = new Date().getTime();
                    time = end - start;
                    resolve(new featureSchema(false, false, [], 'deleteQueueData', time, null, true, "No such queue available."));
                    return;
                }
                let config = {
                    method: 'delete',
                    url: `http://${domain}/api/queues/${vhost}/${queue}/contents`,	
                    headers: {
                        'Authorization': `Basic ${btoa(`${u_name}:${u_password}`)}`,
                        'Content-Type': 'application/json'
                    }
                };
                axios(config).then(function (response) {
                    end = new Date().getTime();
                    time = end - start;
                    resolve(new featureSchema(true, false, response.data, 'deleteQueueData', time, 'Null')); //----->> SuccessEndPoint
                }).catch(function (err2) {
                    let resp = (err2.response == undefined) ? [] : err2.response.data;
                    end = new Date().getTime();
                    time = end - start;
                    resolve(new featureSchema(false, true, resp, 'deleteQueueData', time, err2.toString())); //----->> ErrorEndPoint
                });
        } catch (err1) {
            end = new Date().getTime();
            time = end - start;
            console.log(err1);
            resolve(new featureSchema(false, true, [], 'deleteQueue', time, err1.toString())); //----->> ErrorEndPoint
        }
    });
}
//deleteQueueData('localhost:15672','guest','guest','harry_host','engine_2_publish').then(x=>{ console.log(x); });

module.exports.brokerFeature = allFeatures;