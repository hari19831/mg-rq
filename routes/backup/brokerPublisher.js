'use strict';

var amqp = require('amqplib/callback_api');

function publishSchema(success, limitation, error, result, type, time, errMsg, limitationMsg){
    this.isSuccess      = (success == undefined) ? false : success;
    this.isLimitation   = (limitation == undefined) ? false : limitation;
    this.isError        = (error == undefined) ? false : error;
    this.result         = result || [];
    this.featureType    = type || 'publishMessage';
    this.timeTaken_ms   = time || 0;
    this.errMsg         = errMsg || 'Null';
    this.limitationMsg  = limitationMsg || 'Null';
}

function publishMessage(domain, vhost, u_name, u_password, exchange, routing, queue, payload){

    let start = new Date().getTime(); let end; let time;
    return new Promise((resolve, reject)=>{
        try{
            //console.log(`connectionString: amqp://${u_name}:${u_password}@${domain}/${vhost}`);
            amqp.connect(`amqp://${u_name}:${u_password}@${domain}/${vhost}`, function(error0, connection) {
                if (error0) { throw error0; }
    
                connection.createChannel(function(error1, channel) {
                    if (error1) {
                    throw error1;
                    }
                    // var exchange = 'direct_logs';
                    // var args = process.argv.slice(2);
                    // var msg = args.slice(1).join(' ') || 'Hello World!';
                    // var severity = (args.length > 0) ? args[0] : 'info';
    
                    // channel.assertExchange(exchange, 'direct', {
                    // durable: false
                    // });
                    channel.publish(exchange, routing, Buffer.from(payload));
                    console.log(" [x] Sent %s: '%s'", 'severity', payload);
                });
    
                setTimeout(function() {
                    end = new Date().getTime();
                    time = end - start;
                    resolve( new publishSchema(true, false, false, {result:'...msg delivered...'},'publishMessage', time, 'null','null') ); //----->> SuccessEndPoint
                    connection.close();
                    process.exit(0)
                }, 500);
            });
        }catch(err1){

            end = new Date().getTime();
            time = end - start;
            console.log(err1);
            resolve( new publishSchema(false, false, true, [],'publishMessage', time, err1.toString()) ); //----->> ErrorEndPoint

        }

    });
}

publishMessage('localhost:5672','harry_host','guest','guest', 'harry_fanout','','', 'check msg published to queue').then((msg)=>{ console.log(msg); }); 