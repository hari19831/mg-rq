'use strict';

var amqp = require('amqplib/callback_api');

function consumeSchema(success, limitation, error, result, type, time, errMsg, limitationMsg){
    this.isSuccess      = (success == undefined) ? false : success;
    this.isLimitation   = (limitation == undefined) ? false : limitation;
    this.isError        = (error == undefined) ? false : error;
    this.result         = result || [];
    this.featureType    = type || 'consumeMessage';
    this.timeTaken_ms   = time || 0;
    this.errMsg         = errMsg || 'Null';
    this.limitationMsg  = limitationMsg || 'Null';
}

function consumeMessage(domain, vhost, u_name, u_password, exchange, routing, queue){

    let start = new Date().getTime(); let end; let time;
    return new Promise((resolve, reject)=>{
        try{
            amqp.connect(`amqp://${u_name}:${u_password}@${domain}/${vhost}`, function(error0, connection) {
                if (error0) { throw error0; }
                connection.createChannel(function(error1, channel) {
                  if (error1) { throw error1; }
                    console.log(`...waiting for msgs ${queue}`);
                  channel.consume(queue, function(msg){         
                    end = new Date().getTime();
                    time = end - start;
                    resolve( new consumeSchema(true, false, false, {msg: msg.content.toString()},'consumeMessage', time, 'Null', 'Null') ); //----->> SuccessEndPoint
                    if(msg.content) {
                        console.log(" [x] %s", msg.content.toString());
                    }
                  }, { noAck: false });
                });
              });
        }catch(err1){

            end = new Date().getTime();
            time = end - start;
            console.log(err1);
            resolve( new consumeSchema(false, false, true, [],'consumeMessage', time, err1.toString(),'Null') ); //----->> ErrorEndPoint

        }

    });
}

consumeMessage('localhost:5672','harry_host','guest','guest', 'harry_fanout','','engine_2_publish').then((msg)=>{ console.log(msg); }); 