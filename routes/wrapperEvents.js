var eventEmitter = require('events');

var broker = new eventEmitter();

broker.on('consume', function (text) {
  console.log(`Consumer: ${text}`);
});

broker.on('publish', function (text) {
    console.log(`Producer: ${text}`);
});

broker.emit('consume', 'hello world consumer');
broker.emit('publish', 'hello world publish');
